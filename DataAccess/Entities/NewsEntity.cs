﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class NewsEntity
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Img { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public bool IsNew { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public string DateDelete { get; set; }
        public string UserCreate { get; set; }
        public string UserDelete { get; set; }
        public string UserUpdate { get; set; }
        public int IdGroupNews { get; set; }
        public string UrlVideo { get; set; }
        public int? OrderNews { get; set; }
        public string SeoKeyWord { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDesciption { get; set; }
        public string Position { get; set; }
        public bool Active { get; set; }
    }
}
