﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class AdvertisementEntity
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Link { get; set; }
        public string Desciption { get; set; }
        public string Location { get; set; }
        public int? OrderImg { get; set; }
        public bool Active { get; set; }
    }
}
