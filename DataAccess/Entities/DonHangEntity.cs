﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class DonHangEntity
    {
        public int? Id { get; set; }
        public string TenSanPham { get; set; }
        public int SoLuong { get; set; }
        public double DonGia { get; set; }
        public double TongGia { get; set; }
        public string TenKhachHang { get; set; }
        public string DiaChi { get; set; }
        public string Sdt { get; set; }
        public string GhiChu { get; set; }
        public string LoaiThanhToan { get; set; }
        public DateTime NgayMua { get; set; }
        public bool Active { get; set; }
    }
}
