﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class GroupNewsRepository : GenericRepository<GroupNewsEntity>
    {
        IConnectionFactory _connectionFactory;

        public GroupNewsRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<GroupNewsEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_GroupNews where Active = 1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<GroupNewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<GroupNewsEntity> GetByPosition(string position)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_GroupNews where Position = " + position + " and Active = 1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<GroupNewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public IEnumerable<GroupNewsEntity> GetByFilter(string sortColumn,string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_GroupNews ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<GroupNewsEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public GroupNewsEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_GroupNews WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<GroupNewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(GroupNewsEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                GroupNewsEntity result = connection.Query<GroupNewsEntity>(
                    sql: "INSERT INTO db_GroupNews (Name, Url,Img, Active, Position ) " +
                            " VALUES (@Name, @Url, @Img, @Active, @Position ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(GroupNewsEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                GroupNewsEntity result = connection.Query<GroupNewsEntity>(
                    sql: "UPDATE db_GroupNews SET Name = @Name, Url = @Url, Img = @Img, Active = @Active, Position = @Position" +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                GroupNewsEntity result = connection.Query<GroupNewsEntity>(
                    sql: "Delete db_GroupNews WHERE Id =  "+ Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<GroupNewsEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_GroupNews where Name = '" + name + "' ";
                }
                else
                {
                    query = "SELECT * FROM db_GroupNews ";
                }
                var param = new DynamicParameters();
                var list = connection.Query<GroupNewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

    }
}
