﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class NhomSanPhamRepository : INhomSanPhamRepository
    {
        IConnectionFactory _connectionFactory;

        public NhomSanPhamRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<NhomSanPhamEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_NhomSanPham where Active =1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public List<NhomSanPhamEntity> GetListNoiBat()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_NhomSanPham where HienThiNoiBat=1 order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<NhomSanPhamEntity> GetListTrangChu()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_NhomSanPham where HienThiTrangChu=1 order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public int GetIdByUrl(string url)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT Id FROM db_NhomSanPham where Url='"+ url + "' order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<int>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return list;
            }
        }

        public List<NhomSanPhamEntity> GetTop10()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT TOP 8 FROM db_NhomSanPham order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<NhomSanPhamEntity> GetByFilter(string sortColumn, string sortColumnDir, string Ten, string Active)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_NhomSanPham where 1=1 ");

                if (!string.IsNullOrEmpty(Ten))
                {
                    sqlQuery.Append(@"AND TenNhom LIKE N'%" + Ten + "%' ");
                }

                if (!string.IsNullOrEmpty(Active))
                {
                    if (Active == "1")
                    {
                        sqlQuery.Append(@"AND Active = 1 ");
                    }
                    else
                    {
                        sqlQuery.Append(@"AND Active = 0 ");
                    }
                }

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public NhomSanPhamEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_NhomSanPham WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(NhomSanPhamEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                NhomSanPhamEntity result = connection.Query<NhomSanPhamEntity>(
                    sql: "INSERT INTO db_NhomSanPham (TenNhom, Url, Mota, Img, SeoTitle, SeoDescription, SeoKeyWord, HienThiTrangChu, HienThiNoiBat, Active ) " +
                            " VALUES (@TenNhom, @Url, @Mota, @Img, @SeoTitle, @SeoDescription, @SeoKeyWord, @HienThiTrangChu, @HienThiNoiBat, @Active ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(NhomSanPhamEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                NhomSanPhamEntity result = connection.Query<NhomSanPhamEntity>(
                    sql: "UPDATE db_NhomSanPham SET TenNhom = @TenNhom, Url = @Url, Mota = @Mota, Img =@Img, SeoTitle = @SeoTitle, SeoDescription = @SeoDescription," +
                    "SeoKeyWord = @SeoKeyWord, HienThiTrangChu= @HienThiTrangChu, HienThiNoiBat= @HienThiNoiBat,Active=@Active " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                NhomSanPhamEntity result = connection.Query<NhomSanPhamEntity>(
                    sql: "Delete db_NhomSanPham WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool SetActiveTrue(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                NhomSanPhamEntity result = connection.Query<NhomSanPhamEntity>(
                    sql: "Update db_NhomSanPham set Active = 1 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool SetActiveFalse(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                NhomSanPhamEntity result = connection.Query<NhomSanPhamEntity>(
                    sql: "Update db_NhomSanPham set Active = 0 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<NhomSanPhamEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_NhomSanPham where (TenNhom like N'%" + name + "%' or TenNhom like '%" + name + "%') and Active =1 ";
                }
                else
                {
                    query = "SELECT * FROM db_NhomSanPham where Active = 1";
                }
                var param = new DynamicParameters();
                var list = connection.Query<NhomSanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
    }
}
