﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class AdvertisementRepository : GenericRepository<AdvertisementEntity>
    {
        IConnectionFactory _connectionFactory;

        public AdvertisementRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<AdvertisementEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_advertisement where Active = 1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<AdvertisementEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<AdvertisementEntity> GetByFilter(string sortColumn,string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_advertisement ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<AdvertisementEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public AdvertisementEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_advertisement WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<AdvertisementEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }

        public List<AdvertisementEntity> GetByPosition(string position)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_advertisement WHERE Location = '" + position + "' order by OrderImg";
                var param = new DynamicParameters();
                var list = connection.Query<AdvertisementEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public bool Create(AdvertisementEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                AdvertisementEntity result = connection.Query<AdvertisementEntity>(
                    sql: "INSERT INTO db_advertisement (Title, Img,Link, Desciption, Location, OrderImg,Active ) " +
                            " VALUES (@Title, @Img, @Link, @Desciption, @Location, @OrderImg, @Active ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(AdvertisementEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                AdvertisementEntity result = connection.Query<AdvertisementEntity>(
                    sql: "UPDATE db_advertisement SET Title = @Title, Img = @Img, Link = @Link, Desciption = @Desciption, Location = @Location, OrderImg = @OrderImg, Active = @Active " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                AdvertisementEntity result = connection.Query<AdvertisementEntity>(
                    sql: "Delete db_advertisement WHERE Id =  "+ Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<AdvertisementEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_advertisement where Title = '" + name + "' ";
                }
                else
                {
                    query = "SELECT * FROM db_advertisement ";
                }
                var param = new DynamicParameters();
                var list = connection.Query<AdvertisementEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

    }
}
