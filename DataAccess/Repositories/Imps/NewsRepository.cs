﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class NewsRepository : GenericRepository<NewsEntity>
    {
        IConnectionFactory _connectionFactory;

        public NewsRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<NewsEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_News where Active = 1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<NewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<NewsEntity> GetByPosition(string position)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_News where Position = "+ position + " and Active = 1 order by OrderNews ";
                var param = new DynamicParameters();
                var list = connection.Query<NewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<NewsEntity> SearchTitle(string key)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_News where Title Like '%"+ key + "%' and Active = 1 order by DateCreate ";
                var param = new DynamicParameters();
                var list = connection.Query<NewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<NewsEntity> GetByGroupNews(string IdGroup)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_News where IdGroupNews = " + IdGroup + " and Active = 1 order by OrderNews ";
                var param = new DynamicParameters();
                var list = connection.Query<NewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<NewsEntity> GetByFilter(string IdGroupNews, string Active, string DateCreateFrom, string DateCreateTo, string sortColumn, string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_News where 1=1 ");
                if (!string.IsNullOrEmpty(IdGroupNews))
                {
                    sqlQuery.Append(@"AND IdGroupNews = " + IdGroupNews + " ");
                }
                if (!string.IsNullOrEmpty(DateCreateFrom) && !string.IsNullOrEmpty(DateCreateTo))
                {
                    sqlQuery.Append(@"AND (DateCreate >= '" + DateCreateFrom + "' AND DateCreate <= '" + DateCreateTo + "') ");
                }

                if (!string.IsNullOrEmpty(Active))
                {
                    sqlQuery.Append(@"AND Active = " + Active + " ");
                }
                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<NewsEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }
     
        public NewsEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_News WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<NewsEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(NewsEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)  
            {
                NewsEntity result = connection.Query<NewsEntity>(
                    sql: "INSERT INTO db_News (Title, Url,Img, Description, Detail, IsNew, DateCreate, UserCreate, IdGroupNews, UrlVideo, OrderNews, SeoKeyWord, SeoTitle, SeoDesciption, Active, Position ) " +
                            " VALUES (@Title, @Url, @Img, @Description, @Detail, @IsNew, @DateCreate, @UserCreate, @IdGroupNews, @UrlVideo, @OrderNews, @SeoKeyWord, @SeoTitle, @SeoDesciption, @Active, @Position) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(NewsEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                NewsEntity result = connection.Query<NewsEntity>(
                    sql: "UPDATE db_News SET Title = @Title, Url = @Url, Img = @Img, Description = @Description, Detail = @Detail, IsNew = @IsNew, DateUpdate = @DateUpdate, UserUpdate = @UserUpdate, IdGroupNews = @IdGroupNews," +
                    "UrlVideo = @UrlVideo, OrderNews = @OrderNews, SeoKeyWord = @SeoKeyWord, SeoTitle = @SeoTitle, SeoDesciption = @SeoDesciption, Active = @Active, Position= @Position " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                NewsEntity result = connection.Query<NewsEntity>(
                    sql: "Delete db_News WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<NewsEntity> NewsPage(int currentPage, int rowPerPage, string type, List<NewsEntity> listNewsItem)
        {
            listNewsItem = GetPageBySpQuery2(listNewsItem, currentPage, rowPerPage);
            return listNewsItem;
        }
        public List<NewsEntity> GetPageBySpQuery2(List<NewsEntity> query, int currentPage, int rowPerPage)
        {
            int TongSoBanGhiSauKhiQuery = 0;
            var ltsNewsItem = query; //Lấy về tất cả
            var ltsIdSelect = new List<int>();
            if (ltsNewsItem.Any())
            {
                TongSoBanGhiSauKhiQuery = ltsNewsItem.Count(); // Tổng số lấy về        
                int intBeginFor = (currentPage - 1) * rowPerPage; //index Bản ghi đầu tiên cần lấy trong bảng
                int intEndFor = (currentPage * rowPerPage) - 1; ; //index bản ghi cuối
                intEndFor = (intEndFor > (TongSoBanGhiSauKhiQuery - 1)) ? (TongSoBanGhiSauKhiQuery - 1) : intEndFor; //nếu vượt biên lấy row cuối

                for (int rowIndex = intBeginFor; rowIndex <= intEndFor; rowIndex++)
                {
                    ltsIdSelect.Add(Convert.ToInt32(ltsNewsItem[rowIndex].Id));
                }

            }
            else
                TongSoBanGhiSauKhiQuery = 0;
            //Query listItem theo listID
            var iquery = from c in ltsNewsItem
                         where ltsIdSelect.Contains(int.Parse(c.Id.ToString()))
                         select c;

            return iquery.ToList();
        }
    }
}
