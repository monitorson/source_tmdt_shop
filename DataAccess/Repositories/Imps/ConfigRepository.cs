﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ConfigRepository : GenericRepository<ConfigEntity>
    {
        IConnectionFactory _connectionFactory;

        public ConfigRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public ConfigEntity GetInfo()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Config ";
                var param = new DynamicParameters();
                var entity = connection.Query<ConfigEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Update(ConfigEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                ConfigEntity result = connection.Query<ConfigEntity>(
                    sql: "UPDATE db_Config SET Company = @Company, Phone = @Phone, Email = @Email, Address = @Address," +
                    "LinkFace = @LinkFace, LinkYoutube=@LinkYoutube,GoogleMap =@GoogleMap, LinkTikTok=@LinkTikTok, LinkVideo = @LinkVideo, LinkGoogle=@LinkGoogle," +
                    " EmailInfo= @EmailInfo, Fax = @Fax, Hotline = @Hotline, ContentFooter = @ContentFooter, FanPage= @FanPage," +
                    "SeoTitle = @SeoTitle, SeoKeyWord= @SeoKeyWord, SeoDesciption= @SeoDesciption, CodeAnalytics = @CodeAnalytics, CodeHeader=@CodeHeader " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }
    }
}
