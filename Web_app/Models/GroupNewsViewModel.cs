﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DailyOpt_CROP4A.Validate;
using DataAccess.Entities;

namespace Web_app.Models
{
    public class GroupNewsViewModel
    {
        public int? Id { get; set; }

        [CustomStringLength(500, ErrorMessage = "Tên nhóm tin quá dài")]
        [Required(ErrorMessage = "Tên nhóm tin không để trống")]
        public string Name { get; set; }

        public string Url { get; set; }

        public string Img { get; set; }

        public int? Active { get; set; }
        public string Position { get; set; }
        public string DisplayActive { get; set; }
        public List<PositionEntity> Positions { get; set; } = new List<PositionEntity>();
        
    }
}