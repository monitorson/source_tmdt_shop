﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class SanPhamViewModel
    {
        public int? Id { get; set; }
        public string TenSanPham { get; set; }
        public string Url { get; set; }
        [Required(ErrorMessage = "Bạn không được để trống nhóm sản phẩm, vui lòng chọn!")]
        public string IdNhomSanPham { get; set; }
        public string Img { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeyWord { get; set; }
        public bool Active { get; set; }
        public string Mota { get; set; }
        public string ChiTiet { get; set; }
        public string GiaBan { get; set; }
        public string GiaKhuyenMai { get; set; }
        public string DisplayNhomSanPham { get; set; }
        public string UrlNhomSanPham { get; set; }
        public string SoLuong { get; set; }

        public NhomSanPhamEntity NhomSanPham = new NhomSanPhamEntity();
        public List<NhomSanPhamEntity> NhomSanPhams = new List<NhomSanPhamEntity>();
    }
}