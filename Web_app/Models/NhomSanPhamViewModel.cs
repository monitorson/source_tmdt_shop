﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class NhomSanPhamViewModel
    {
        public int? Id { get; set; }
        public string TenNhom { get; set; }
        public string Url { get; set; }
        public string Mota { get; set; }
        public string Img { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeyWord { get; set; }
        public bool HienThiTrangChu { get; set; }
        public bool HienThiNoiBat { get; set; }
        public bool Active { get; set; }
        public List<SanPhamEntity> lstSanPham = new List<SanPhamEntity>();
    }
}