﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class ConfigViewModel
    {
        public int? Id { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string LinkFace { get; set; }
        public string LinkYoutube { get; set; }
        public string GoogleMap { get; set; }
        public string LinkTikTok { get; set; }
        public string LinkVideo { get; set; }
        public string LinkGoogle { get; set; }
        public string EmailInfo { get; set; }
        public string Fax { get; set; }
        public string Hotline { get; set; }
        public string ContentFooter { get; set; }
        public string FanPage { get; set; }
        public string SeoTitle { get; set; }
        public string SeoKeyWord { get; set; }
        public string SeoDesciption { get; set; }
        public string CodeAnalytics { get; set; }
        public string CodeHeader { get; set; }

    }
}