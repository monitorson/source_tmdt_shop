﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web_app.Models
{
    public class Select2ViewModel
    {
        public string id { get; set; }
        public string text { get; set; }
    }
}