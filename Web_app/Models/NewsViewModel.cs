﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using DailyOpt_CROP4A.Validate;
using DataAccess.Entities;

namespace Web_app.Models
{
    public class NewsViewModel
    {
        public int? Id { get; set; }

        [CustomStringLength(500, ErrorMessage = "Tên bài tin quá dài")]
        [Required(ErrorMessage = "Tên bài tin không để trống")]
        public string Title { get; set; }
        public string Url { get; set; }
        public string Img { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public int? IsNew { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public string DateDelete { get; set; }
        public string UserCreate { get; set; }
        public string UserDelete { get; set; }
        public string UserUpdate { get; set; }
        public int IdGroupNews { get; set; }
        public string UrlVideo { get; set; }
        public int? OrderNews { get; set; }
        public string SeoKeyWord { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDesciption { get; set; }
        public int? Active { get; set; }
        public string DisplayGroupNews { get; set; }
        public string Position { get; set; }
        public string UrlGroupNews { get; set; }
        public List<GroupNewsEntity> GroupNews { get; set; } = new List<GroupNewsEntity>();
        public List<PositionEntity> Positions { get; set; } = new List<PositionEntity>();

    }
}