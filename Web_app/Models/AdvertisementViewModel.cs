﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using DailyOpt_CROP4A.Validate;
using DataAccess.Entities;

namespace Web_app.Models
{
    public class AdvertisementViewModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Link { get; set; }
        public string Desciption { get; set; }
        public string Location { get; set; }
        public int? OrderImg { get; set; }
        public int? Active { get; set; }
    }
}