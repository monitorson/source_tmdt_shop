﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web_app
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
              name: "DetailNews",
              "Detail/{cate}/{type}",
              new { controller = "News", action = "ChiTietBaiViet" },
              new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
            "Contact",
            "lien-he",
            new { controller = "Home", action = "LienHe" },
            new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
         "Dieukhoan",
         "dieu-khoan-su-dung",
         new { controller = "Home", action = "DieuKhoan" },
         new[] { "Code.Web.Controllers" }
         );
            routes.MapRoute(
           "Search",
           "tim-kiem",
           new { controller = "News", action = "TimKiem" },
           new[] { "Code.Web.Controllers" }
           );

            routes.MapRoute(
         "ThemDiaDiem",
         "them-dia-diem",
         new { controller = "Review", action = "ThemDiaDiem" },
         new[] { "Code.Web.Controllers" }
         );
            routes.MapRoute(
       "addiaDiem",
       "review/add-dia-diem",
       new { controller = "Review", action = "ThemDiaDiem" },
       new[] { "Code.Web.Controllers" }
       );
            routes.MapRoute(
  "TimKiem",
  "Home/tim-kiem",
  new { controller = "Home", action = "Search" },
  new[] { "Code.Web.Controllers" }
  );
            routes.MapRoute(
"GioHang",
"gio-hang",
new { controller = "SanPham", action = "GioHang" },
new[] { "Code.Web.Controllers" }
);
            routes.MapRoute(
"ThanhToan",
"thanh-toan",
new { controller = "SanPham", action = "ThanhToan" },
new[] { "Code.Web.Controllers" }
);
            routes.MapRoute(
"listSPTheoNhom",
"product/{type}",
new { controller = "SanPham", action = "DanhSachSanPham" },
new[] { "Code.Web.Controllers" }
);
            routes.MapRoute(
"chiTietSanPham",
"product/{type}/{url}",
new { controller = "SanPham", action = "ChiTietSanPham" },
new[] { "Code.Web.Controllers" }
);
            routes.MapRoute(
      "admingianhang",
      "Admin_GianHang/Add-GianHang",
      new { controller = "Admin_GianHang", action = "GianHang" },
      new[] { "Code.Web.Controllers" }
      );

            routes.MapRoute(
              "GroupNews",
              "{type}",
              new { controller = "News", action = "DanhSachBaiViet" },
              new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
            "PhanTrang",
            "page/News/PartialNews",
            new { controller = "News", action = "PartialNews" },
            new[] { "Code.Web.Controllers" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

           


        }
    }
}
