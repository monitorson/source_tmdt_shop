﻿
$(function () {
    $("#ListMenu").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_Menu/show-menu",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Title", "name": "Title" },
            {
                "orderable": false,
                "data": null,
                "name": "Img",
                render: function (data, type, row) {
                    if (data.Img === null || data.Img === "") {
                        return '<img src="/Img/no-image-100.png" style="max-height: 100px;" />';
                    }
                    else {
                        return '<img src="' + data.Img + '" style="max-height: 100px;" />';
                    }
                },
                "targets": -1
            },
            { "data": "Link", "name": "Link" },
            { "data": "Location", "name": "Location" },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteMenu(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListMenu').DataTable();
    $('#ListMenu tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Menu/get-info-menu',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-menu').modal('show');
                $("#Location").val(result.result.Location);
                $("input[name=Title]").val(result.result.Title);
                $("#Location").val(result.result.Location);
                $("#ParentId").empty();
                var defaultnewOption = $("<option></option>").val("0").text("--- Chọn menu cha ---")
                $("#ParentId").append(defaultnewOption).trigger('change');
                for (i = 0; i < result.result.Parents.length; ++i) {
                    var $newOption = $("<option></option>").val(result.result.Parents[i].Id).text(result.result.Parents[i].Title)
                    $("#ParentId").append($newOption).trigger('change');
                }
                $("#ParentId").val(result.result.ParentId);
                if (result.result.Link !== null) {
                    $("#KieuLK").val("1");
                    $('#LinkURL').css('display', 'block');
                    $('#NoiDung').css('display', 'none');
                    $('#LienKet').css('display', 'none');
                }
                if (result.result.Detail !== null) {
                    $("#KieuLK").val("2");
                    $('#LinkURL').css('display', 'none');
                    $('#NoiDung').css('display', 'block');
                    $('#LienKet').css('display', 'none');
                }
                if (result.result.Url !== null) {
                    $("#KieuLK").val("3");
                    $('#LinkURL').css('display', 'none');
                    $('#NoiDung').css('display', 'none');
                    $('#LienKet').css('display', 'block');
                }
                $("input[name=Link]").val(result.result.Link);
                $("#Url").val(result.result.Url);
                $("#AnhDaiDien").attr('src', result.result.Img);
                $("#Image").val(result.result.Img);
                if (result.result.Active === 1) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
                CKEDITOR.instances['Detail'].setData(result.result.Detail);
                $("input[name=OrderMenu]").val(result.result.OrderMenu);
            }
        });

    });


});

function DeleteMenu(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Menu/delete-menu',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListMenu').DataTable();
                    table.draw();
                }

            }
        });
    }
}
function SelectMenu(value) {
    //Kiểu link sang trang khác
    if (value === "1") {
        $('#LinkURL').css('display', 'block');
        $('#NoiDung').css('display', 'none');
        $('#LienKet').css('display', 'none');
        $("input[name=Link]").val("");
        $("#Url").val("");
        $("textarea[name=Detail]").val("");
    }
    //Kiểu nội dung
    if (value === "2") {
        $('#LinkURL').css('display', 'none');
        $('#NoiDung').css('display', 'block');
        $('#LienKet').css('display', 'none');
        $("input[name=Link]").val("");
        $("#Url").val("");
        $("textarea[name=Detail]").val("");
    }
    //Kiểu liên kết nhóm tin
    if (value === "3") {
        $('#LinkURL').css('display', 'none');
        $('#NoiDung').css('display', 'none');
        $('#LienKet').css('display', 'block');
        $("input[name=Link]").val("");
        $("#Url").val("");
        $("textarea[name=Detail]").val("");
    }
}