﻿
$(function () {
    $("#ListNhomSanPham").DataTable({
        "lengthNhomSanPham": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthNhomSanPham": "Hiển thị _NhomSanPham_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_SanPham/show-NhomSanPham",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
                return $.extend({}, d, {
                    "TenNhom": $("#TenSearch").val(),
                    "Active": $("#ActiveSearch").val(),
                });
            }
        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "TenNhom", "name": "TenNhom" },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-check-circle" style="color:red;cursor: pointer;" onclick="ActiveNhomSanPham(' + data.Id + ');" title="Active"></i> <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteNhomSanPham(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListNhomSanPham').DataTable();
    $('#ListNhomSanPham tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_SanPham/get-info-NhomSanPham',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-NhomSanPham').modal('show');
                $("#TenNhom").val(result.result.TenNhom);
                $("input[name=Url]").val(result.result.Url);
                CKEDITOR.instances["Mota"].setData(result.result.Mota);
                $("#AnhDaiDien").attr('src', result.result.Img);
                $("#Image").val(result.result.Img);
                $("#SeoTitle").val(result.result.SeoTitle);
                $("#SeoDescription").val(result.result.SeoDescription);
                $("#SeoKeyWord").val(result.result.SeoKeyWord);
                if (result.result.Active === true) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
                if (result.result.HienThiTrangChu === true) {
                    $("#HienThiTrangChu").prop("checked", true);
                }
                else {
                    $("#HienThiTrangChu").prop("checked", false);
                }
                if (result.result.HienThiNoiBat === true) {
                    $("#HienThiNoiBat").prop("checked", true);
                }
                else {
                    $("#HienThiNoiBat").prop("checked", false);
                }
            }
        });

    });


});
function SetUrl(value) {
    var str = xoa_dau_tieng_viet(value);
    var res = str.replace(/\s+/g, '-').toLowerCase();
    $("input[name=Url]").val(res);
    $("#SeoTitle").val(value);
    $("#SeoDescription").val(value);
    $("#SeoKeyWord").val(value);
}
function DeleteNhomSanPham(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_SanPham/delete-NhomSanPham',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListNhomSanPham').DataTable();
                    table.draw();
                }

            }
        });
    }
}

function ActiveNhomSanPham(Id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_SanPham/active-NhomSanPham',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                $("#MesageDelete").css("display", "block");
                var table = $('#ListNhomSanPham').DataTable();
                table.draw();
            }

        }
    });
}

function ShowModalAdd() {
    $("#TenNhom").val("");
    $("input[name=Url]").val("");
    CKEDITOR.instances["Mota"].setData("");
    $("#AnhDaiDien").attr('src', "");
    $("#Image").val("");
    $("#SeoTitle").val("");
    $("#SeoDescription").val("");
    $("#SeoKeyWord").val("");
    $("#Active").prop("checked", true);
    $("#HienThiTrangChu").prop("checked", false);
    $("#HienThiNoiBat").prop("checked", false);
    $('#modal-NhomSanPham').modal('show');
}

function Search() {
    var table = $('#ListNhomSanPham').DataTable();
    table.draw();
} 