﻿$(document).ready(function () {
    $(".chose-group-sp").select2({
        ajax: {
            url: '/Admin_DonHang/show-nhom-sp',
            width: 'resolve',
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,
                    requestForm: "edit"
                };
            },
            processResults: function (data) {
                return { results: data.DataList };
            }
        }
    });
});
$(function () {
    $("#ListDonHang").DataTable({
        "lengthDonHang": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthDonHang": "Hiển thị _DonHang_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_DonHang/show-DonHang",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
                return $.extend({}, d, {
                    "TenDonHang": $("#TenSearch").val(),
                    "Active": $("#ActiveSearch").val(),
                    "DateFrom": $("#DateFromSearch").val(),
                    "DateTo": $("#DateToSearch").val(),
                });
            }
        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "TenSanPham", "name": "TenSanPham" },
            { "data": "TenKhachHang", "name": "TenKhachHang" },
            { "data": "Sdt", "name": "Sdt" },
            {
                "orderable": false,
                "data": null,
                "name": "TongGia",
                render: function (data, type, row) {
                    if (data.TongGia != null || data.TongGia != "") {
                        return data.TongGia.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " VNĐ";
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-check-circle" style="color:red;cursor: pointer;" onclick="ActiveDonHang(' + data.Id + ');" title="Active"></i> <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteDonHang(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListDonHang').DataTable();
    $('#ListDonHang tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_DonHang/get-info-DonHang',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-DonHang').modal('show');
                $("#Id").val(result.result.Id);
                $("#TenSanPham").text(result.result.TenSanPham);
                $("#SoLuong").text(result.result.SoLuong);
                $("#DonGia").text(formatPrice_Edit(result.result.DonGia));
                $("#TongGia").text(formatPrice_Edit(result.result.TongGia) + "VNĐ");
                $("#DiaChi").text(result.result.DiaChi);
                $("#Sdt").text(result.result.Sdt);
                $("#NgayMua").text(my_date_format(result.result.NgayMua));
                $("textarea[id=GhiChu]").val(result.result.GhiChu);
                $("#TenKhachHang").text(result.result.TenKhachHang);
                $("#LoaiThanhToan").text(result.result.LoaiThanhToan);
                if (result.result.Active === true) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
            }
        });

    });


});
function DeleteDonHang(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_DonHang/delete-DonHang',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListDonHang').DataTable();
                    table.draw();
                }

            }
        });
    }
}

function ActiveDonHang(Id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_DonHang/active-DonHang',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                $("#MesageDelete").css("display", "block");
                var table = $('#ListDonHang').DataTable();
                table.draw();
            }

        }
    });
}

function Search() {
    var table = $('#ListDonHang').DataTable();
    table.draw();
}

function formatPrice_Edit(price) {
   return price.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatPrice(input) {
    var price = input.value;
    input.value = price.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

var my_date_format = function (input) {
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'];
    var date = d.getDate() + " " + month[d.getMonth()] + " năm " + d.getFullYear();
    var time = d.toLocaleTimeString().toLowerCase().replace(/([\d]+:[\d]+):[\d]+(\s\w+)/g, "$1$2");
    return ("Ngày " + date + " - Lúc " + time);
};
$('#btnExport').click(function () {
    var id = $("#Id").val();
    if (id != null && id != "") {
        var data = {
            Id: id,
        };
        var dataToSend = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_DonHang/ExportExcel',
            dataType: "json",
            data: dataToSend,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                window.location = '/Admin_DonHang/Download';
            }
        });
    }
});

$('#Export_time').click(function () {
    var time_from = $("#DateFromSearch").val();
    var time_to = $("#DateToSearch").val();
    var active = $("#ActiveSearch").val();
    
        var data = {
            DateFrom: time_from,
            DateTo: time_to,
            Active: active
        };
        var dataToSend = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_DonHang/ExportExcel_Time',
            dataType: "json",
            data: dataToSend,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                window.location = '/Admin_DonHang/Download';
            }
        });
});
