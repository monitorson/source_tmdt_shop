﻿function xoa_dau_tieng_viet(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // loại bỏ toàn bộ dấu space (nếu có) ở 2 đầu của xâu
    str.trim();
    return str;
}
//function BrowseServer() {
//    var finder = new CKFinder();
//    finder.selectActionFunction = SetFileField;
//    finder.popup();
//}
//function SetFileField(fileUrl) {
//    document.getElementById('Image').value = fileUrl;
//    $('#AnhDaiDien').attr('src', fileUrl);
//}

// updload 1 ảnh
$("#uploadImg").change(function () {
    var files = $('#uploadImg')[0].files;
    for (var i = 0; i < files.length; i++) {
        var formData = new FormData();
        formData.append('file', files[i]);

        $.ajax({
            type: 'POST',
            url: '/HomeAdmin/ProcessUpload',
            contentType: false,
            processData: false,
            data: formData,
            success: function (urlImage) {
                $("#Image").val(urlImage);
                $('#AnhDaiDien').attr('src', urlImage);
            },
            error: function (err) {
                alert('Có lỗi khi Upload' + err.statusText);
            }
        }).done(function () {
            i++;
        });;
    }
});

// updload nhiều ảnh
$("#upload").change(function () {
    $("#ListImgView").empty();
    $("#ListImg").val("");
    var files = $('#upload')[0].files;
    for (var i = 0; i < files.length; i++) {
        var formData = new FormData();
        formData.append('file', files[i]);

        $.ajax({
            type: 'POST',
            url: '/HomeAdmin/UploadMultiImage',
            contentType: false,
            processData: false,
            data: formData,
            success: function (urlImage) {
                $("#ListImg").val($("#ListImg").val() + urlImage + ",");
                $("#ListImgView").css("display", "block");
                $("#ListImgView").append("<img id='img" + i + "' src='" + urlImage + "' class='img-album' style='max-width:100%;max-height:100px;margin: 10px;float: left;' /><button type='button' style='border: none; background: none;float: left;' id='ok" + i + "' title='" + urlImage + "' onclick='deleteImgAlbum(\"" + i + "\");'><i class='ti-close'></i></button>")
            },
            error: function (err) {
                alert('Có lỗi khi Upload' + err.statusText);
            }
        }).done(function () {
            i++;
        });;
    }
});
function deleteImgAlbum(valable) {
    var linkImg = $('#ok' + valable + '').prop('title');
    $("#ListImgView #img" + valable + "").remove();
    $("#ListImgView #ok" + valable + "").remove();
    linkImg = linkImg + ",";
    $("#ListImg").val($("#ListImg").val().replace("" + linkImg + "", ""));
}

