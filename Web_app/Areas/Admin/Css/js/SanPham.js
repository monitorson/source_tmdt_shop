﻿$(document).ready(function () {
    $(".chose-group-sp").select2({
        ajax: {
            url: '/Admin_SanPham/show-nhom-sp',
            width: 'resolve',
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,
                    requestForm: "edit"
                };
            },
            processResults: function (data) {
                return { results: data.DataList };
            }
        }
    });
});
$(function () {
    $("#ListSanPham").DataTable({
        "lengthSanPham": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthSanPham": "Hiển thị _SanPham_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_SanPham/show-SanPham",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
                return $.extend({}, d, {
                    "TenSanPham": $("#TenSearch").val(),
                    "Active": $("#ActiveSearch").val(),
                });
            }
        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "TenSanPham", "name": "TenSanPham" },
            {
                "orderable": false,
                "data": null,
                "name": "AnhDaiDien",
                render: function (data, type, row) {
                    var listImg = data.Img.split(",");
                    return '<img src="' + listImg[0] + '" style="width:80px;height:80px;object-fit: contain;" />';
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-check-circle" style="color:red;cursor: pointer;" onclick="ActiveSanPham(' + data.Id + ');" title="Active"></i> <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteSanPham(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListSanPham').DataTable();
    $('#ListSanPham tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_SanPham/get-info-SanPham',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-SanPham').modal('show');
                $("#TenSanPham").val(result.result.TenSanPham);
                $("input[name=Url]").val(result.result.Url);
                CKEDITOR.instances["Mota"].setData(result.result.Mota);
                CKEDITOR.instances["ChiTiet"].setData(result.result.ChiTiet);
                var $newOption = $("<option selected='selected'></option>").val(result.result.IdNhomSanPham).text(result.result.DisplayNhomSanPham)
                $("#IdNhomSanPham").append($newOption).trigger('change');
                $("#ListImgView").empty();
                $("#ListImg").val("");

                var listImg = result.result.Img.split(",");
                listImg.splice(-1, 1);
                for (var i = 0; i < listImg.length; i++) {
                    $("#ListImgView").css("display", "block");
                    $("#ListImg").val($("#ListImg").val() + listImg[i] + ",");
                    $("#ListImgView").append("<img id='img" + i + "' src='" + listImg[i] + "' class='img-album' style='max-width:100%;max-height:100px;margin: 10px;float: left;' /><button type='button' style='border: none; background: none;float: left;' id='ok" + i + "' title='" + listImg[i] + "' onclick='deleteImgAlbum(\"" + i + "\");'><i class='ti-close'></i></button>")
                }
                $("#GiaKhuyenMai").val(formatPrice_Edit(result.result.GiaKhuyenMai));
                $("#GiaBan").val(formatPrice_Edit(result.result.GiaBan));
                $("#SeoTitle").val(result.result.SeoTitle);
                $("#SeoDescription").val(result.result.SeoDescription);
                $("#SeoKeyWord").val(result.result.SeoKeyWord);
                if (result.result.Active === true) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
            }
        });

    });


});
function SetUrl(value) {
    var str = xoa_dau_tieng_viet(value);
    var res = str.replace(/\s+/g, '-').toLowerCase();
    $("input[name=Url]").val(res);
    $("#SeoTitle").val(value);
    $("#SeoDescription").val(value);
    $("#SeoKeyWord").val(value);
}
function DeleteSanPham(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_SanPham/delete-SanPham',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListSanPham').DataTable();
                    table.draw();
                }

            }
        });
    }
}

function ActiveSanPham(Id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_SanPham/active-SanPham',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                $("#MesageDelete").css("display", "block");
                var table = $('#ListSanPham').DataTable();
                table.draw();
            }

        }
    });
}

function ShowModalAdd() {
    $("#Id").val("");
    $("#TenSanPham").val("");
    $("input[name=Url]").val("");
    CKEDITOR.instances["Mota"].setData("");
    CKEDITOR.instances["ChiTiet"].setData("");
    var $newOption = $("<option selected='selected'></option>").val("").text("")
    $("#IdNhomSanPham").append($newOption).trigger('change');
    $("#ListImgView").empty();
    $("#ListImg").val("");
    $("#SeoTitle").val("");
    $("#SeoDescription").val("");
    $("#SeoKeyWord").val("");
    $("#Active").prop("checked", true);
    $("#GiaKhuyenMai").val("");
    $("#GiaBan").val("");
    $('#modal-SanPham').modal('show');
}

function Search() {
    var table = $('#ListSanPham').DataTable();
    table.draw();
}

function formatPrice_Edit(price) {
   return price.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatPrice(input) {
    var price = input.value;
    input.value = price.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}