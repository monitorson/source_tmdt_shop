﻿$(function () {
    $("#ListGroupNews").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        //"scrollY": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_News/show-nhom-tin",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Name", "name": "Name" },
            { "data": "Url", "name": "Url" },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Img === null || data.Img === "") {
                        return '<img src="/Img/no-image-100.png" style="max-height: 100px;" />';
                    }
                    else {
                        return '<img src="' + data.Img + '" style="max-height: 100px;" />';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
             {
                "orderable": false,
                "data": null,
                "name": "Delete",
                 render: function (data, type, row) {
                     return '<i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteGroupNews(' + data.Id + ');"></i>';
                },
                "targets": -1
            }
        ]
    });
});
 $(document).ready(function () {
     var table = $('#ListGroupNews').DataTable();
     $('#ListGroupNews tbody').on('click', 'td:not(:last-child)', function () {
         if ($(this).hasClass('selected')) {
             $(this).removeClass('selected');
         }
         else {
             table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
         var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_News/get-info-groupnews',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-group-news').modal('show');
                $("input[name=Name]").val(result.result.Name);
                $("input[name=Url]").val(result.result.Url);
                $("#AnhDaiDien").attr('src', result.result.Img);
                $("#Image").val(result.result.Img);
                $("#Position").val(result.result.Position);
                if (result.result.Active === 1) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
            }
        });

    });

     $('#btnUpload').click(function (e) {
         $('#fileUpload').trigger('click');
     });
     $('#fileUpload').change(function () {
         if (window.FormData !== undefined) {
             var fileUpload = $('#fileUpload').get(0);
             var files = fileUpload.files;
             var formData = new FormData();
             formData.append('file', files[0]);
             $.ajax({
                 type: 'POST',
                 url: '/Admin_News/ProcessUpload',
                 contentType: false,
                 processData: false,
                 data: formData,
                 success: function (urlImage) {
                     $('#AnhDaiDien').attr('src', urlImage);
                     $('#Img').val(urlImage);
                 },
                 error: function (err) {
                     alert('Có lỗi xảy ra khi Upload' + err.statusText);
                 }
             });
         }
     });
 });
function SetUrl(value) {
    var str = xoa_dau_tieng_viet(value);
    var res = str.replace(/\s+/g, '-').toLowerCase();
    $("input[name=Url]").val(res);
}
function ShowModalAdd() {
    $("input[Id=Id]").val("");
    $("input[name=Name]").val("");
    $("input[name=Url]").val("");
    $("#AnhDaiDien").attr('src', "");
    $("#Image").val("");
    $("#Active").prop("checked", true);
    $("#Position").val("0");
    $('#modal-group-news').modal('show');
}
function DeleteGroupNews(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_News/delete-group-news',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListGroupNews').DataTable();
                    table.draw();
                }

            }
        });
    }
}

