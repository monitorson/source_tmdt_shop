﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;
using DailyOpt_CROP4A.Common.Mapper;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Admin_SanPham")]
    [TrackMenuItem("management.Admin_SanPham")]
    public class Admin_SanPhamController : Controller
    {
        private ISanPhamRepository _SanPhamRepository = null;
        private INhomSanPhamRepository _NhomSanPhamRepository = null;
        public Admin_SanPhamController(INhomSanPhamRepository NhomSanPhamRepository, ISanPhamRepository SanPhamRepository)
        {
            _NhomSanPhamRepository = NhomSanPhamRepository;
            _SanPhamRepository = SanPhamRepository;
        }
        // GET: Admin/Admin_NhomSanPham
        public ActionResult IndexNhomSanPham()
        {
            var model = new NhomSanPhamViewModel();
            return View(model);
        }
        public ActionResult IndexSanPham()
        {
            var model = new SanPhamViewModel();
            model.NhomSanPhams = _NhomSanPhamRepository.GetAll();
            return View(model);
        }
        // show nhóm sản phẩm cho dropdown list select 2

        [HttpPost]
        [Route("show-nhom-sp", Name = "ManagementGroupSpPost")]
        public ActionResult Group_sp_Read(string name, string requestForm)
        {
            try
            {

                List<Select2ViewModel> DataList = new List<Select2ViewModel>();
                Select2ViewModel data;

                var dataRole = _NhomSanPhamRepository.GetListByName(name);

                if (string.IsNullOrEmpty(requestForm))
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.TenNhom;
                        DataList.Add(data);
                    }
                }
                else
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.TenNhom;
                        DataList.Add(data);
                    }
                }
                Select2ViewModel datanull = new Select2ViewModel();
                datanull.id = "";
                datanull.text = "";
                DataList.Add(datanull);
                return Json(new { DataList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Group_News_Read: " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("show-NhomSanPham", Name = "ManagementNhomSanPhamPost")]
        public ActionResult NhomSanPham_Read(string TenNhom, string Active)
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _NhomSanPhamRepository.GetByFilter(sortColumn, sortColumnDir, TenNhom, Active);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("NhomSanPham_Read: " + ex.Message);
                throw;
            }

        }
        [HttpPost]
        [Route("show-SanPham", Name = "ManagementSanPhamPost")]
        public ActionResult SanPham_Read(string TenSanPham, string Active)
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _SanPhamRepository.GetByFilter(sortColumn, sortColumnDir, TenSanPham, Active);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("SanPham_Read: " + ex.Message);
                throw;
            }

        }


        [Route("get-info-NhomSanPham", Name = "ManagementGetInfoNhomSanPhamPost")]
        public ActionResult NhomSanPham_Preview(string Id)
        {
            try
            {
                var ma = int.Parse(Id);
                var data = _NhomSanPhamRepository.GetByNo(Id);
                var dataList = NhomSanPhamMapper.ConvertNhomSanPhamEntityToModel(data);
                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("NhomSanPham_Preview: " + ex.Message);
                throw;
            }

        }

        [Route("get-info-SanPham", Name = "ManagementGetInfoSanPhamPost")]
        public ActionResult SanPham_Preview(string Id)
        {
            try
            {
                var ma = int.Parse(Id);
                var data = _SanPhamRepository.GetByNo(Id);
                var dataList = SanPhamMapper.ConvertSanPhamEntityToModel(data);
                dataList.DisplayNhomSanPham = _NhomSanPhamRepository.GetByNo(dataList.IdNhomSanPham).TenNhom;
                return Json(new { result = dataList });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("SanPham_Preview: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_NhomSanPham(NhomSanPhamViewModel model)
        {
            try
            {
                var entity = NhomSanPhamMapper.ConvertNhomSanPhamModelToEntity(model);
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var img = _NhomSanPhamRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-NhomSanPham");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-NhomSanPham");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-NhomSanPham");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var img = _NhomSanPhamRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-NhomSanPham");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-NhomSanPham");
                        }
                    }
                    else
                    {
                        var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-NhomSanPham");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_NhomSanPham: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-NhomSanPham");
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_SanPham(SanPhamViewModel model)
        {
            try
            {
                var entity = SanPhamMapper.ConvertSanPhamModelToEntity(model);
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    model.GiaBan = model.GiaBan.ToString().Replace(",","");
                    model.GiaKhuyenMai =model.GiaKhuyenMai.ToString().Replace(",", "");
                    if (ModelState.IsValid)
                    {
                        var img = _SanPhamRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-SanPham");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-SanPham");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-SanPham");
                    }
                }
                else
                {
                    model.GiaBan = model.GiaBan.ToString().Replace(",", "");
                    model.GiaKhuyenMai = model.GiaKhuyenMai.ToString().Replace(",", "");
                    //update
                    if (ModelState.IsValid)
                    {
                        var img = _SanPhamRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-SanPham");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-SanPham");
                        }
                    }
                    else
                    {
                        var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-SanPham");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_SanPham: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-SanPham");
            }

        }

        [HttpPost]
        [Route("delete-NhomSanPham", Name = "ManagementDeleteNhomSanPham")]
        public ActionResult Delete_NhomSanPham(string Id)
        {
            try
            {
                var listSp = _SanPhamRepository.GetByIdNhom(Id);
                foreach (var item in listSp)
                {
                    _SanPhamRepository.Delete(item.Id.ToString());
                }
                var data = _NhomSanPhamRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_NhomSanPham: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("delete-SanPham", Name = "ManagementDeleteSanPham")]
        public ActionResult Delete_SanPham(string Id)
        {
            try
            {
                var data = _SanPhamRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_SanPham: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("active-NhomSanPham", Name = "ManagementActiveNhomSanPham")]
        public ActionResult Active_NhomSanPham(string Id)
        {
            try
            {
                var obj = _NhomSanPhamRepository.GetByNo(Id);
                if (obj.Active == true)
                {
                    _NhomSanPhamRepository.SetActiveFalse(Id);
                }
                else
                {
                    _NhomSanPhamRepository.SetActiveTrue(Id);
                }
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Active_NhomSanPham: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("active-SanPham", Name = "ManagementActiveSanPham")]
        public ActionResult Active_SanPham(string Id)
        {
            try
            {
                var obj = _SanPhamRepository.GetByNo(Id);
                if (obj.Active == true)
                {
                    _SanPhamRepository.SetActiveFalse(Id);
                }
                else
                {
                    _SanPhamRepository.SetActiveTrue(Id);
                }
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Active_SanPham: " + ex.Message);
                throw;
            }

        }
    }
}