﻿using DataAccess.Entities;
using DataAccess.Repositories;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Admin_News")]
    [TrackMenuItem("management.Admin_News")]
    public class Admin_NewsController : Controller
    {

        private GroupNewsRepository _groupNewsRepository = null;
        private NewsRepository _newsRepository = null;
        private PositionRepository _positionRepository = null;
        public Admin_NewsController(GroupNewsRepository groupNewsRepository, NewsRepository newsRepository, PositionRepository positionRepository)
        {
            _groupNewsRepository = groupNewsRepository;
            _newsRepository = newsRepository;
            _positionRepository = positionRepository;

        }

        // GET: Admin/Admin_News
        public ActionResult NhomTin(GroupNewsViewModel model)
        {
            model.Positions = _positionRepository.GetAll();
            return View(model);
        }
        public ActionResult BaiTin(NewsViewModel model)
        {
            model.Positions = _positionRepository.GetAll();
            model.GroupNews = _groupNewsRepository.GetListByName(null);
            return View(model);
        }


        [HttpPost]
        [Route("show-nhom-tin", Name = "ManagementGroupNewsPost")]
        public ActionResult Group_News_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _groupNewsRepository.GetByFilter(sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Group_News_Read: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("show-bai-tin", Name = "ManagementNewsPost")]
        public ActionResult News_Read(string IdGroupNews, string Active, string DateCreateFrom, string DateCreateTo)
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _newsRepository.GetByFilter(IdGroupNews, Active, DateCreateFrom, DateCreateTo, sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("News_Read: " + ex.Message);
                throw;
            }

        }


        [Route("get-info-groupnews", Name = "ManagementGetInfoPost")]
        public ActionResult GroupNews_Preview(string Id)
        {
            try
            {
                var data = _groupNewsRepository.GetByNo(Id);

                var dataList = ParseEntityToModel(data);

                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("GroupNews_Preview: " + ex.Message);
                throw;
            }

        }
        [Route("get-info-news", Name = "ManagementGetInfoNewsPost")]
        public ActionResult News_Preview(string Id)
        {
            try
            {
                var data = _newsRepository.GetByNo(Id);

                var dataList = ParseNewsEntityToModel(data);

                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("News_Preview: " + ex.Message);
                throw;
            }

        }

        private GroupNewsViewModel ParseEntityToModel(GroupNewsEntity entity)
        {
            try
            {
                var groupNewsViewModel = new GroupNewsViewModel
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Url = entity.Url,
                    Img = entity.Img,
                    Position = entity.Position,
                    Active = entity.Active == true ? 1 : 0,
                };
                return groupNewsViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }

        private NewsViewModel ParseNewsEntityToModel(NewsEntity entity)
        {
            try
            {
                var nameGroupNews = _groupNewsRepository.GetByNo(entity.IdGroupNews.ToString()).Name;
                var newsViewModel = new NewsViewModel
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Url = entity.Url,
                    Img = entity.Img,
                    DateCreate = entity.DateCreate,
                    DateDelete = entity.DateDelete,
                    DateUpdate = entity.DateUpdate,
                    Description = entity.Description,
                    Detail = entity.Detail,
                    IdGroupNews = entity.IdGroupNews,
                    IsNew = entity.IsNew == true ? 1 : 0,
                    OrderNews = entity.OrderNews,
                    SeoDesciption = entity.SeoDesciption,
                    SeoKeyWord = entity.SeoKeyWord,
                    SeoTitle = entity.SeoTitle,
                    UrlVideo = entity.UrlVideo,
                    UserCreate = entity.UserCreate,
                    UserDelete = entity.UserDelete,
                    UserUpdate = entity.UserUpdate,
                    DisplayGroupNews = nameGroupNews,
                    Position = entity.Position,
                    Active = entity.Active == true ? 1 : 0,
                };
                return newsViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("add-group", Name = "ManagementGroupNewsAddPost")]
        public ActionResult Add_Group_News(GroupNewsViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var entity = new GroupNewsEntity();
                        entity.Name = model.Name;
                        entity.Img = model.Img;
                        entity.Url = model.Url;
                        entity.Position = model.Position;
                        entity.Active = model.Active == 0 ? true : false;
                        var groupNews = _groupNewsRepository.Create(entity);
                        if (groupNews)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-GroupNews");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-GroupNews");
                        }
                    }
                    else
                    {
                        //var errors = ModelState.Select(x => x.Value.Errors)
                        //   .Where(y => y.Count > 0)
                        //   .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-GroupNews");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new GroupNewsEntity();
                        entity.Id = model.Id;
                        entity.Name = model.Name;
                        entity.Img = model.Img;
                        entity.Url = model.Url;
                        entity.Position = model.Position;
                        entity.Active = model.Active == 0 ? true : false;
                        var groupNews = _groupNewsRepository.Update(entity);
                        if (groupNews)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-GroupNews");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-GroupNews");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-GroupNews");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_Group_News: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-GroupNews");
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_News(NewsViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                var nameUser = "";
                if (!string.IsNullOrEmpty(Session["Fullname"].ToString()))
                {
                    nameUser = Session["Fullname"].ToString();
                }
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var entity = new NewsEntity();
                        entity.Title = model.Title;
                        entity.Url = model.Url;
                        entity.Img = model.Img;
                        entity.DateCreate = comon.GetCurrentDateTime().ToString("yyyy/MM/dd");
                        entity.Description = model.Description;
                        entity.Detail = model.Detail;
                        entity.IdGroupNews = model.IdGroupNews;
                        entity.IsNew = model.IsNew == 0 ? true : false;
                        entity.OrderNews = model.OrderNews;
                        entity.SeoDesciption = model.SeoDesciption;
                        entity.SeoKeyWord = model.SeoKeyWord;
                        entity.SeoTitle = model.SeoTitle;
                        entity.UrlVideo = model.UrlVideo;
                        entity.UserCreate = nameUser;
                        entity.Position = model.Position;
                        entity.Active = model.Active == 0 ? true : false;
                        var news = _newsRepository.Create(entity);
                        if (news)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Post-News");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Post-News");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Post-News");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new NewsEntity();
                        entity.Id = model.Id;
                        entity.Title = model.Title;
                        entity.Url = model.Url;
                        entity.Img = model.Img;
                        entity.DateUpdate = comon.GetCurrentDateTime().ToString("yyyy/MM/dd");
                        entity.Description = model.Description;
                        entity.Detail = model.Detail;
                        entity.IdGroupNews = model.IdGroupNews;
                        entity.IsNew = model.IsNew == 0 ? true : false;
                        entity.OrderNews = model.OrderNews;
                        entity.SeoDesciption = model.SeoDesciption;
                        entity.SeoKeyWord = model.SeoKeyWord;
                        entity.SeoTitle = model.SeoTitle;
                        entity.UrlVideo = model.UrlVideo;
                        entity.UserUpdate = nameUser;
                        entity.Position = model.Position;
                        entity.Active = model.Active == 0 ? true : false;
                        var news = _newsRepository.Update(entity);
                        if (news)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Post-News");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Post-News");
                        }
                    }
                    else
                    {
                        var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Post-News");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_News: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-Post-News");
            }

        }

        [HttpPost]
        public string ProcessUpload(HttpPostedFileBase file)
        {
            file.SaveAs(Server.MapPath(@"\Uploads\News\" + file.FileName));
            return @"\Uploads\News\" + file.FileName;
        }

        [HttpPost]
        [Route("delete-group-news", Name = "ManagementDeleteGroupNews")]
        public ActionResult Delete_GroupNews(string Id)
        {
            try
            {
                var data = _groupNewsRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_GroupNews: " + ex.Message);
                throw;
            }

        }


        [HttpPost]
        [Route("delete-news", Name = "ManagementDeleteNews")]
        public ActionResult Delete_News(string Id)
        {
            try
            {
                var data = _newsRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_News: " + ex.Message);
                throw;
            }

        }


        [HttpPost]
        [Route("coppy-news", Name = "ManagementCoppyNews")]
        public ActionResult Coppy_News(string Id)
        {
            try
            {
                var model = _newsRepository.GetByNo(Id);
                var entity = new NewsEntity();
                entity.Title = model.Title;
                entity.Url = model.Url;
                entity.Img = model.Img;
                entity.DateCreate = comon.GetCurrentDateTime().ToString("yyyy/MM/dd");
                entity.Description = model.Description;
                entity.Detail = model.Detail;
                entity.IdGroupNews = model.IdGroupNews;
                entity.IsNew = model.IsNew;
                entity.OrderNews = model.OrderNews;
                entity.SeoDesciption = model.SeoDesciption;
                entity.SeoKeyWord = model.SeoKeyWord;
                entity.SeoTitle = model.SeoTitle;
                entity.UrlVideo = model.UrlVideo;
                entity.UserCreate = model.UserCreate;
                entity.Active = model.Active;
                entity.Position = model.Position;
                var data = _newsRepository.Create(entity);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Coppy_News: " + ex.Message);
                throw;
            }

        }
        
        public ActionResult Group_News_Read(string name, string requestForm)
        {
            try
            {

                List<Select2ViewModel> DataList = new List<Select2ViewModel>();
                Select2ViewModel data;

                var dataRole = _groupNewsRepository.GetListByName(name);

                if (string.IsNullOrEmpty(requestForm))
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.Name;
                        DataList.Add(data);
                    }
                }
                else
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.Name;
                        DataList.Add(data);
                    }
                }
                Select2ViewModel datanull = new Select2ViewModel();
                datanull.id = "";
                datanull.text = "";
                DataList.Add(datanull);
                return Json(new { DataList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Group_News_Read: " + ex.Message);
                throw;
            }
        }
    }
}