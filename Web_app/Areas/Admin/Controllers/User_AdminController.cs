﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("User_Admin")]
    [TrackMenuItem("management.User_Admin")]
    public class User_AdminController : Controller
    {
        private UserAdminRepository _userAdminRepository = null;
      
        public User_AdminController(UserAdminRepository userAdminRepository)
        {
            _userAdminRepository = userAdminRepository;
        }

        // GET: Admin/User_Admin
        public ActionResult ListUser(UserAdminViewModel model)
        {
            model.Roles = _userAdminRepository.GetListRole(null);
            return View(model);
        }

        [HttpPost]
        [Route("show-list-user", Name = "ManagementListUserPost")]
        public ActionResult User_Admin_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _userAdminRepository.GetByFilter(sortColumn, sortColumnDir);

                var list = new List<UserAdminViewModel>();
                foreach (var item in listData)
                {
                    list.Add(ParseEntityToModel(item));
                }
                //total number of rows count     
                recordsTotal = list.Count();
                //Paging     
                var datamodel = list.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("User_Admin_Read: " + ex.Message);
                throw;
            }

        }
        [Route("get-info-user", Name = "ManagementGetInfoUserPost")]
        public ActionResult Admin_User_Preview(string Id)
        {
            try
            {
                var data = _userAdminRepository.GetByNo(Id);
                var dataList = ParseEntityToModel(data);
                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Admin_User_Preview: " + ex.Message);
                throw;
            }

        }
        private UserAdminViewModel ParseEntityToModel(UserAdminEntity entity)
        {
            try
            {
                var nameRole = _userAdminRepository.GetRole(int.Parse(entity.Role.ToString())).NameRole;
                var userAdminViewModel = new UserAdminViewModel
                {
                    Id = entity.Id,
                    DisplayRole = nameRole,
                    Email = entity.Email,
                    Fullname = entity.Fullname,
                    Password = entity.Password,
                    Phone = entity.Phone,
                    Role = entity.Role,
                    Username = entity.Username
                };
                return userAdminViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("add-user", Name = "ManagementUserAdminAddPost")]
        public ActionResult Add_User_Admin(UserAdminViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {
                    if (ModelState.IsValid)
                    {
                        if(_userAdminRepository.GetByEmail(model.Email) != null)
                        {
                            TempData["MessageError"] = "Email đã có người đăng ký";
                            return RedirectToAction("Add-User-Admin");
                        }
                        else
                        {
                            var entity = new UserAdminEntity();
                            entity.Fullname = model.Fullname;
                            entity.Email = model.Email;
                            entity.Password = model.Password;
                            entity.Phone = model.Phone;
                            entity.Role = model.Role;
                            entity.Username = model.Username;
                            var groupNews = _userAdminRepository.Create(entity);
                            if (groupNews)
                            {
                                TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                                return RedirectToAction("Add-User-Admin");
                            }
                            else
                            {
                                //insert error
                                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                                return RedirectToAction("Add-User-Admin");
                            }
                        }
                    }
                    else
                    {
                        //var errors = ModelState.Select(x => x.Value.Errors)
                        //   .Where(y => y.Count > 0)
                        //   .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-User-Admin");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new UserAdminEntity();
                        entity.Id = model.Id;
                        entity.Fullname = model.Fullname;
                        entity.Email = model.Email;
                        entity.Password = model.Password;
                        entity.Phone = model.Phone;
                        entity.Role = model.Role;
                        entity.Username = model.Username;
                        var groupNews = _userAdminRepository.Update(entity);
                        if (groupNews)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-User-Admin");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-User-Admin");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-User-Admin");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_User_Admin: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-User");
            }

        }

        [HttpPost]
        [Route("delete-user-admin", Name = "ManagementDeleteUserAdmin")]
        public ActionResult Delete_GroupNews(string Id)
        {
            try
            {
                var data = _userAdminRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_GroupNews: " + ex.Message);
                throw;
            }

        }

        public ActionResult Role_Read(string name, string requestForm)
        {
            try
            {

                List<Select2ViewModel> DataList = new List<Select2ViewModel>();
                Select2ViewModel data;

                var dataRole = _userAdminRepository.GetListRole(name);

                if (string.IsNullOrEmpty(requestForm))
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.NameRole;
                        DataList.Add(data);
                    }
                }
                else
                {
                    foreach (var selected in dataRole)
                    {
                        data = new Select2ViewModel();
                        data.id = selected.Id.ToString();
                        data.text = selected.Id + " - " + selected.NameRole;
                        DataList.Add(data);
                    }
                }
                Select2ViewModel datanull = new Select2ViewModel();
                datanull.id = "";
                datanull.text = "";
                DataList.Add(datanull);
                return Json(new { DataList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Role_Read: " + ex.Message);
                throw;
            }
        }

    }
}