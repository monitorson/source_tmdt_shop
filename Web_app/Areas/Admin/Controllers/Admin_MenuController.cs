﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Admin_Menu")]
    [TrackMenuItem("management.Admin_Menu")]
    public class Admin_MenuController : Controller
    {

        private GroupNewsRepository _groupNewsRepository = null;
        private MenuRepository _menuRepository = null;
        public Admin_MenuController(GroupNewsRepository groupNewsRepository, MenuRepository menuRepository)
        {
            _groupNewsRepository = groupNewsRepository;
            _menuRepository = menuRepository;
        }
        // GET: Admin/Admin_Menu
        public ActionResult Menu(MenuViewModel model)
        {
            model.Urls = _groupNewsRepository.GetAll().Where(c => c.Active == true).ToList();
            model.Parents = _menuRepository.GetAll().Where(c => c.ParentId == 0).ToList();
            ViewBag.ListParents = model.Parents;
            return View(model);
        }

        [HttpPost]
        [Route("show-menu", Name = "ManagementMenuPost")]
        public ActionResult Menu_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _menuRepository.GetByFilter(sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Img_Read: " + ex.Message);
                throw;
            }

        }


        [Route("get-info-menu", Name = "ManagementGetInfoMenuPost")]
        public ActionResult Menu_Preview(string Id)
        {
            try
            {
                var ma = int.Parse(Id);
                var data = _menuRepository.GetByNo(Id);

                var dataList = ParseEntityToModel(data);
                dataList.Parents = _menuRepository.GetAll().Where(c => c.ParentId == 0 && c.Id != ma).ToList();
                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Menu_Preview: " + ex.Message);
                throw;
            }

        }

        private MenuViewModel ParseEntityToModel(MenuEntity entity)
        {
            try
            {
                var MenuViewModel = new MenuViewModel
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Detail = entity.Detail,
                    Img = entity.Img,
                    Link = entity.Link,
                    Location = entity.Location,
                    OrderMenu = entity.OrderMenu,
                    ParentId = entity.ParentId,
                    Url = entity.Url,
                    Active = entity.Active == true ? 1 : 0,
                };
                return MenuViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_Menu(MenuViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var entity = new MenuEntity();
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Link = model.Link;
                        entity.Active = model.Active == 0 ? true : false;
                        entity.Location = model.Location;
                        entity.OrderMenu = model.OrderMenu;
                        entity.Detail = model.Detail;
                        entity.ParentId = model.ParentId;
                        entity.Url = model.Url;
                        var img = _menuRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Menu");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Menu");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Menu");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new MenuEntity();
                        entity.Id = model.Id;
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Link = model.Link;
                        entity.Active = model.Active == 0 ? true : false;
                        entity.Location = model.Location;
                        entity.OrderMenu = model.OrderMenu;
                        entity.Detail = model.Detail;
                        entity.ParentId = model.ParentId;
                        entity.Url = model.Url;
                        var img = _menuRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Menu");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Menu");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Menu");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_Menu: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-Menu");
            }

        }

        [HttpPost]
        [Route("delete-menu", Name = "ManagementDeleteMenu")]
        public ActionResult Delete_Menu(string Id)
        {
            try
            {
                var data = _menuRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_Menu: " + ex.Message);
                throw;
            }

        }
    }
}