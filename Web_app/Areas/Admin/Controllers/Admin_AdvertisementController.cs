﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Admin_Advertisement")]
    [TrackMenuItem("management.Admin_Advertisement")]
    public class Admin_AdvertisementController : Controller
    {

        private AdvertisementRepository _advertisementRepository = null;
        public Admin_AdvertisementController(AdvertisementRepository advertisementRepository)
        {
            _advertisementRepository = advertisementRepository;

        }
        // GET: Admin/Admin_Img
        public ActionResult Advertisement()
        {
            return View();
        }

        [HttpPost]
        [Route("show-img", Name = "ManagementImgPost")]
        public ActionResult Img_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _advertisementRepository.GetByFilter(sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Img_Read: " + ex.Message);
                throw;
            }

        }


        [Route("get-info", Name = "ManagementGetInfoImgPost")]
        public ActionResult Img_Preview(string Id)
        {
            try
            {
                var data = _advertisementRepository.GetByNo(Id);

                var dataList = ParseEntityToModel(data);

                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Img_Preview: " + ex.Message);
                throw;
            }

        }

        private AdvertisementViewModel ParseEntityToModel(AdvertisementEntity entity)
        {
            try
            {
                var AdvertisementViewModel = new AdvertisementViewModel
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Desciption = entity.Desciption,
                    Img = entity.Img,
                    Link = entity.Link,
                    Location = entity.Location,
                    OrderImg = entity.OrderImg,
                    Active = entity.Active == true ? 1 : 0,
                };
                return AdvertisementViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_Img(AdvertisementViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var entity = new AdvertisementEntity();
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Link = model.Link;
                        entity.Active = model.Active == 0 ? true : false;
                        entity.Location = model.Location;
                        entity.OrderImg = model.OrderImg;
                        entity.Desciption = model.Desciption;
                        var img = _advertisementRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Img");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Img");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Img");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new AdvertisementEntity();
                        entity.Id = model.Id;
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Link = model.Link;
                        entity.Active = model.Active == 0 ? true : false;
                        entity.Location = model.Location;
                        entity.OrderImg = model.OrderImg;
                        entity.Desciption = model.Desciption;
                        var img = _advertisementRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Img");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Img");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Img");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_Img: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-Img");
            }

        }

        [HttpPost]
        [Route("delete-img", Name = "ManagementDeleteImg")]
        public ActionResult Delete_Img(string Id)
        {
            try
            {
                var data = _advertisementRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_Img: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("coppy-img", Name = "ManagementCoppyImg")]
        public ActionResult Coppy_Img(string Id)
        {
            try
            {
                var model = _advertisementRepository.GetByNo(Id);
                var entity = new AdvertisementEntity();
                entity.Title = model.Title;
                entity.Link = model.Link;
                entity.Img = model.Img;
                entity.Desciption = model.Desciption;
                entity.Location = model.Location;
                entity.OrderImg = model.OrderImg;
                entity.Active = model.Active;
                var data = _advertisementRepository.Create(entity);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Coppy_Img: " + ex.Message);
                throw;
            }

        }

    }
}