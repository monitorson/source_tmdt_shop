﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Utilities;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using Web_app.Common;
using DataAccess.Repositories;
using Web_app.Models;
using DataAccess.Entities;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Home_Admin")]
    [TrackMenuItem("management.Home_Admin")]
    public class HomeAdminController : Controller
    {
        private UserAdminRepository _userAdminRepository = null;
        private ConfigRepository _configRepository = null;
        private PositionRepository _positionRepository = null;
        public HomeAdminController(UserAdminRepository userAdminRepository, ConfigRepository configRepository, PositionRepository positionRepository)
        {
            _userAdminRepository = userAdminRepository;
            _configRepository = configRepository;
            _positionRepository = positionRepository;
        }

        // GET: Admin/Home
        public ActionResult IndexAdmin()
        {
            return View();
        }
        public ActionResult CauHinh(ConfigViewModel model)
        {
            var entity = _configRepository.GetInfo();
            model.Id = entity.Id;
            model.Address = entity.Address;
            model.CodeAnalytics = entity.CodeAnalytics;
            model.CodeHeader = entity.CodeHeader;
            model.Company = entity.Company;
            model.ContentFooter = entity.ContentFooter;
            model.Email = entity.Email;
            model.EmailInfo = entity.EmailInfo;
            model.FanPage = entity.FanPage;
            model.Fax = entity.Fax;
            model.GoogleMap = entity.GoogleMap;
            model.Hotline = entity.Hotline;
            model.LinkFace = entity.LinkFace;
            model.LinkGoogle = entity.LinkGoogle;
            model.LinkTikTok = entity.LinkTikTok;
            model.LinkVideo = entity.LinkVideo;
            model.LinkYoutube = entity.LinkYoutube;
            model.Phone = entity.Phone;
            model.SeoDesciption = entity.SeoDesciption;
            model.SeoKeyWord = entity.SeoKeyWord;
            model.SeoTitle = entity.SeoTitle;
            return View(model);
        }
        public ActionResult ViTri()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Change_Info_Config(ConfigViewModel entity)
        {
            try
            {
                //update
                var model = new ConfigEntity();
                model.Id = entity.Id;
                model.Address = entity.Address;
                model.CodeAnalytics = entity.CodeAnalytics;
                model.CodeHeader = entity.CodeHeader;
                model.Company = entity.Company;
                model.ContentFooter = entity.ContentFooter;
                model.Email = entity.Email;
                model.EmailInfo = entity.EmailInfo;
                model.FanPage = entity.FanPage;
                model.Fax = entity.Fax;
                model.GoogleMap = entity.GoogleMap;
                model.Hotline = entity.Hotline;
                model.LinkFace = entity.LinkFace;
                model.LinkGoogle = entity.LinkGoogle;
                model.LinkTikTok = entity.LinkTikTok;
                model.LinkVideo = entity.LinkVideo;
                model.LinkYoutube = entity.LinkYoutube;
                model.Phone = entity.Phone;
                model.SeoDesciption = entity.SeoDesciption;
                model.SeoKeyWord = entity.SeoKeyWord;
                model.SeoTitle = entity.SeoTitle;
                var config = _configRepository.Update(model);
                if (config)
                {
                    TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                    return RedirectToAction("Change-Info-Config");
                }
                else
                {
                    //insert error
                    TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                    return RedirectToAction("Change-Info-Config");
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Change_Info_Config: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Change-Info-Config");
            }

        }

        public ActionResult LoginAdmin()
        {
            if (Session["Fullname"] != null)
            {
                return RedirectToAction("IndexAdmin", "HomeAdmin");
            }
            else
            {
                return View();

            }
        }
        public ActionResult FogotPassword()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginAdmin(UserAdminViewModel model)
        {
            var obj = _userAdminRepository.CheckLogin(model.Username, model.Password);
            if (obj != null)
            {
                Session["IdUser"] = obj.Id;
                Session["Fullname"] = obj.Fullname;
                Session["Role"] = obj.Role;
                return View("IndexAdmin", model);
            }
            else
            {
                TempData["Message"] = "Tài khoản hoặc mật khẩu sai!";
                return View("LoginAdmin", model);
            }

        }

        public ActionResult Logout()
        {
            Session["Fullname"] = null;
            Session["Role"] = null;
            TempData["Message"] = null;
            return RedirectToAction("LoginAdmin", "HomeAdmin");
        }

        [HttpPost]
        public ActionResult Check_Pass(string oldPass)
        {
            try
            {
                var Id = Session["IdUser"].ToString();

                var entity = _userAdminRepository.GetByNo(Id);
                if (entity.Password == oldPass)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Check_Pass: " + ex.Message);
                throw;
            }

        }
        [HttpPost]
        public ActionResult Change_Password(UserAdminViewModel model)
        {
            try
            {
                //update pass
                var entity = new UserAdminEntity();
                entity.Id = model.Id;
                entity.Password = model.Password;
                var user = _userAdminRepository.UpdatePass(entity);
                if (user)
                {
                    TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                    return RedirectToAction("Change-Pass");
                }
                else
                {
                    //insert error
                    TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                    return RedirectToAction("Change-Pass");
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Change_Password: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Change-Pass");
            }

        }
        [HttpPost]
        public ActionResult Fogot(string Email)
        {
            try
            {
                //update pass
                if (_userAdminRepository.GetByEmail(Email) != null)
                {
                    string passNew = RandomString(10);
                    var entity = new UserAdminEntity();
                    entity.Password = passNew;
                    entity.Email = Email;
                    var user = _userAdminRepository.UpdatePassByEmail(entity);
                    // send email
                    SendMail(Email, passNew);
                     TempData["MessageSuccess"] = "Vui lòng kiểm tra email để nhận mật khẩu mới";
                }
                else
                {
                    TempData["MessageSuccess"] = "Email bạn nhập chưa có trong hệ thống";
                }
                return RedirectToAction("Fogot-Pass");
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Fogot: " + ex.Message);
                TempData["MessageSuccess"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Fogot-Pass");
            }

        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        static async Task SendMail(string emailTo, string newPass)
        {
            // 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
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("phamminhson0994@gmail.com", "Thông báo");
            var subject = "Thông báo thay đổi mật khẩu quản trị website";
            var to = new EmailAddress(emailTo, "Quản trị viên");
            var plainTextContent = "";
            var htmlContent = "<strong>Đây là mật khẩu mới của bạn: </strong> "+ newPass + " ";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }

        [HttpPost]
        public ActionResult Position_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _positionRepository.GetByFilter(sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Position_Read: " + ex.Message);
                throw;
            }

        }
        [HttpPost]
        public ActionResult Position_Preview(string Id)
        {
            try
            {
                var data = _positionRepository.GetByNo(Id);

                var dataList = ParseEntityPositionToModel(data);

                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Position_Preview: " + ex.Message);
                throw;
            }

        }
        private PositionViewModel ParseEntityPositionToModel(PositionEntity entity)
        {
            try
            {
                var posViewModel = new PositionViewModel
                {
                    Id = entity.Id,
                    Name = entity.Name,
                };
                return posViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityPositionToModel: " + ex.Message);
                throw;
            }
        }


        [HttpPost]
        public ActionResult Add_Position(PositionViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {
                        var entity = new PositionEntity();
                        entity.Name = model.Name;
                        var data = _positionRepository.Create(entity);
                        if (data)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Position");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Position");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Position");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new PositionEntity();
                        entity.Id = model.Id;
                        entity.Name = model.Name;
                        var data = _positionRepository.Update(entity);
                        if (data)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Position");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Position");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Position");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_Position: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-Position");
            }

        }

        [HttpPost]
        public ActionResult Delete_Position(string Id)
        {
            try
            {
                var data = _positionRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_Position: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        public string ProcessUpload(HttpPostedFileBase file)
        {
            file.SaveAs(Server.MapPath(@"\Uploads\Image\" + file.FileName));
            return @"\Uploads\Image\" + file.FileName;
        }
        [HttpPost]
        public string UploadMultiImage(HttpPostedFileBase file)
        {
            file.SaveAs(Server.MapPath(@"\Uploads\Product\" + file.FileName));
            return @"\Uploads\Product\" + file.FileName;
        }
    }
}