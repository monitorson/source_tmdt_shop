﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;
using DailyOpt_CROP4A.Common.Mapper;
using System.IO;
using OfficeOpenXml;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    [RoutePrefix("Admin_DonHang")]
    [TrackMenuItem("management.Admin_DonHang")]
    public class Admin_DonHangController : Controller
    {
        private IDonHangRepository _DonHangRepository = null;
        private INhomSanPhamRepository _NhomSanPhamRepository = null;
        public Admin_DonHangController(INhomSanPhamRepository NhomSanPhamRepository, IDonHangRepository DonHangRepository)
        {
            _NhomSanPhamRepository = NhomSanPhamRepository;
            _DonHangRepository = DonHangRepository;
        }

        public ActionResult IndexDonHang()
        {
            var model = new DonHangViewModel();
            return View(model);
        }

        [HttpPost]
        [Route("show-DonHang", Name = "ManagementDonHangPost")]
        public ActionResult DonHang_Read(string TenDonHang, string Active, string DateFrom, string DateTo)
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _DonHangRepository.GetByFilter(sortColumn, sortColumnDir, TenDonHang, Active, DateFrom, DateTo);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("DonHang_Read: " + ex.Message);
                throw;
            }

        }

        [Route("get-info-DonHang", Name = "ManagementGetInfoDonHangPost")]
        public ActionResult DonHang_Preview(string Id)
        {
            try
            {
                var ma = int.Parse(Id);
                var data = _DonHangRepository.GetByNo(Id);
                var dataList = DonHangMapper.ConvertDonHangEntityToModel(data);
                return Json(new { result = dataList });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("DonHang_Preview: " + ex.Message);
                throw;
            }

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_DonHang(DonHangViewModel model)
        {
            try
            {
                var entity = DonHangMapper.ConvertDonHangModelToEntity(model);
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    model.DonGia = model.DonGia.ToString().Replace(",", "");
                    model.TongGia = model.TongGia.ToString().Replace(",", "");
                    if (ModelState.IsValid)
                    {
                        var img = _DonHangRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-DonHang");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-DonHang");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-DonHang");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var img = _DonHangRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-DonHang");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-DonHang");
                        }
                    }
                    else
                    {
                        var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-DonHang");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_DonHang: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-DonHang");
            }

        }

        [HttpPost]
        [Route("delete-DonHang", Name = "ManagementDeleteDonHang")]
        public ActionResult Delete_DonHang(string Id)
        {
            try
            {
                var data = _DonHangRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_DonHang: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        [Route("active-DonHang", Name = "ManagementActiveDonHang")]
        public ActionResult Active_DonHang(string Id)
        {
            try
            {
                var obj = _DonHangRepository.GetByNo(Id);
                if (obj.Active == true)
                {
                    _DonHangRepository.SetActiveFalse(Id);
                }
                else
                {
                    _DonHangRepository.SetActiveTrue(Id);
                }
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Active_DonHang: " + ex.Message);
                throw;
            }

        }
        public ActionResult ExportExcel(string Id)
        {
            try
            {
                var file = new FileInfo(Server.MapPath("/Content/template/format_donhang.xlsx"));
                var data = _DonHangRepository.GetByNo(Id);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(file))
                {
                    var worksheet1 = package.Workbook.Worksheets[0];
                    worksheet1.Cells["C2:I3"].Value = data.TenKhachHang;
                    worksheet1.Cells["C4:I5"].Value = data.DiaChi;
                    worksheet1.Cells["C6:I7"].Value = data.Sdt;
                    worksheet1.Cells["C8:I9"].Value = data.TenSanPham;
                    worksheet1.Cells["C10:I11"].Value = data.SoLuong;
                    worksheet1.Cells["C12:I13"].Value = comon.FormatPrice(data.DonGia) + " VNĐ";
                    worksheet1.Cells["C14:I15"].Value = comon.ConvertDate(data.NgayMua);
                    worksheet1.Cells["C16:I22"].Value = data.GhiChu;
                    worksheet1.Cells["C23:I24"].Value = comon.FormatPrice(data.TongGia) + " VNĐ";
                    worksheet1.Cells["F26:I26"].Value = "Ngày xuất: " + comon.ConvertDate(DateTime.Now);
                    package.Workbook.Properties.Title = "Thông tin đơn hàng";
                    package.Workbook.Properties.Author = "Sơn béo";
                    Session["DownloadExcel_FileManager"] = package.GetAsByteArray();
                    Session["FileName"] = "Thông tin đơn hàng.xlsx";
                    return Json("", JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult ExportExcel_Time(string DateFrom, string DateTo, string Active)
        {
            try
            {
                var file = new FileInfo(Server.MapPath("/Content/template/format_donhang_time.xlsx"));
                var data = _DonHangRepository.GetByTime(DateFrom, DateTo, Active);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(file))
                {
                    var worksheet1 = package.Workbook.Worksheets[0];
                    worksheet1.Cells["C3:D3"].Value = comon.ConvertDate(DateTime.Parse(DateFrom));
                    worksheet1.Cells["H3:I3"].Value = comon.ConvertDate(DateTime.Parse(DateTo));
                    for (int i = 0; i < data.Count(); i++)
                    {
                        worksheet1.Cells["A" + (6 + i) + ""].Value = i + 1;
                        worksheet1.Cells["B" + (6 + i) + ""].Value = data[i].TenSanPham;
                        worksheet1.Cells["C" + (6 + i) + ""].Value = data[i].TenKhachHang;
                        worksheet1.Cells["D" + (6 + i) + ""].Value = data[i].Sdt;
                        worksheet1.Cells["E" + (6 + i) + ""].Value = data[i].DiaChi;
                        worksheet1.Cells["F" + (6 + i) + ""].Value = data[i].SoLuong;
                        worksheet1.Cells["G" + (6 + i) + ""].Value = comon.FormatPrice(data[i].DonGia);
                        worksheet1.Cells["H" + (6 + i) + ""].Value = comon.FormatPrice(data[i].TongGia);
                        worksheet1.Cells["I" + (6 + i) + ""].Value = comon.ConvertDate(data[i].NgayMua);
                    }
                    package.Workbook.Properties.Title = "Thông tin đơn hàng theo ngày";
                    package.Workbook.Properties.Author = "Sơn béo";
                    Session["DownloadExcel_FileManager"] = package.GetAsByteArray();
                    Session["FileName"] = "Thông tin đơn hàng theo ngày.xlsx";
                    return Json("", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public ActionResult Download()
        {
            try
            {
                if (Session["DownloadExcel_FileManager"] != null)
                {
                    byte[] data = Session["DownloadExcel_FileManager"] as byte[];
                    return File(data, "application/octet-stream", Session["FileName"].ToString());
                }
                else
                {
                    return new EmptyResult();
                }
            }

            catch (Exception ex)
            {
                Log4NetCommon.Error("Download: " + ex.Message);
                throw;
            }
        }
    }
}