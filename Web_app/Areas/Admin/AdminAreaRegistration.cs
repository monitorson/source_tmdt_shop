﻿using System.Web.Mvc;

namespace DailyOpt_CROP4A.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               "Add_GroupNews",
               "Admin_News/Add-GroupNews",
               new { controller = "Admin_News", action = "NhomTin", id = UrlParameter.Optional }
           );
            context.MapRoute(
              "Add_Position",
              "HomeAdmin/Add-Position",
              new { controller = "HomeAdmin", action = "ViTri", id = UrlParameter.Optional }
          );

            context.MapRoute(
             "Add_News",
             "Admin_News/Add-Post-News",
             new { controller = "Admin_News", action = "BaiTin", id = UrlParameter.Optional }
         );
            context.MapRoute(
          "Change-Config",
          "HomeAdmin/Change-Info-Config",
          new { controller = "HomeAdmin", action = "CauHinh", id = UrlParameter.Optional }
      );
            context.MapRoute(
            "Fogot-Pass",
            "HomeAdmin/Fogot-Pass",
            new { controller = "HomeAdmin", action = "FogotPassword", id = UrlParameter.Optional }
        );
            context.MapRoute(
          "suaGH",
          "Admin_SanPham/Add-NhomSanPham",
          new { controller = "Admin_SanPham", action = "IndexNhomSanPham", id = UrlParameter.Optional }
      );
            context.MapRoute(
        "sp",
        "Admin_SanPham/Add-SanPham",
        new { controller = "Admin_SanPham", action = "IndexSanPham", id = UrlParameter.Optional }
    );
            context.MapRoute(
   "dh",
   "Admin_DonHang/Add-DonHang",
   new { controller = "Admin_DonHang", action = "IndexDonHang", id = UrlParameter.Optional }
);
            context.MapRoute(
             "Change-Pass",
             "HomeAdmin/Change-Pass",
             new { controller = "HomeAdmin", action = "ChangePassword", id = UrlParameter.Optional }
         );
            context.MapRoute(
              "Add-Img-Advertisement",
              "Admin_Advertisement/Add-Img",
              new { controller = "Admin_Advertisement", action = "Advertisement", id = UrlParameter.Optional }
          );
            context.MapRoute(
            "Add_User_Admin",
            "User_Admin/Add-User-Admin",
            new { controller = "User_Admin", action = "ListUser", id = UrlParameter.Optional }
        );
            context.MapRoute(
           "Add_Menu_Admin",
           "Admin_Menu/Add-Menu",
           new { controller = "Admin_Menu", action = "Menu", id = UrlParameter.Optional }
        );

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "HomeAdmin", action = "LoginAdmin", id = UrlParameter.Optional }
            );

            context.MapRoute(
              "quen-mat-khau",
              "quen-mat-khau",
              new { controller = "HomeAdmin", action = "FogotPassword", id = UrlParameter.Optional }
          );

            context.MapRoute(
             "doi-mat-khau",
             "doi-mat-khau",
             new { controller = "HomeAdmin", action = "ChangePassword", id = UrlParameter.Optional }
         );

            context.MapRoute(
            "dang-xuat",
            "dang-xuat",
            new { controller = "HomeAdmin", action = "Logout", id = UrlParameter.Optional }
        );




        }
    }
}