﻿$(document).ready(function () {
    $(".chose-customer").select2({
        ajax: {
            url: '/Experiment/Customer_Read',
            width: 'resolve',
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term
                };
            },
            processResults: function (data) {
                return { results: data.DataList };
            }
        }
    });

});

// function check input chỉ nhập số
$.fn.inputFilter = function (inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });
};

// Check các input chỉ được phép nhập số
$("#客先希望工賃, #見積工賃").inputFilter(function (value) {
    return /^\d*$/.test(value);
});
