﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Web_app.Common
{
    public class comon : ApiController
    {
        /// <summary>
        /// Function to convert time zone Japanese
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private static DateTime ConvertDateTimeToJst(DateTime target)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(target.ToUniversalTime(), TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time"));
        }

        /// <summary>
        /// Get current Date and Time
        /// </summary>
        /// <returns></returns>
        public static DateTime GetCurrentDateTime()
        {
            return ConvertDateTimeToJst(DateTime.Now);
        }

        public static string GetDate(DateTime date)
        {
            var day = (DateTime.Now - date).Days;
            var hour = (DateTime.Now - date).Hours;
            var minute = (DateTime.Now - date).Minutes;
            var second = (DateTime.Now - date).Seconds;

            var result = second + " giây trước";

            if (minute > 0)
            {
                result = minute + " phút trước";
            }

            if (hour > 0)
            {
                result = hour + " giờ trước";
            }

            if (day > 0)
            {
                result = day + " ngày trước";
            }

            if (day > 30)
            {
                result = "Vào ngày " + date.ToString("dd/MM/yyyy");
            }

            return result;
        }

        public static string FormatPrice(decimal? price)
        {
            if (price.HasValue && price != 0)
            {
                string result = price.Value.ToString("#,#");
                if (result.Contains(","))
                    result = result.Replace(',', ',');
                return result;
            }
            return "0";
        }
        public static string ConvertDate(DateTime Date)
        {
            return ConvertDate(Date, "dd/MM/yyyy");
        }
        public static string ConvertDate(DateTime Date, string DateFormat)
        {
            if (Date != null)
            {
                return Date.ToString(DateFormat);
            }
            else
            {
                return "";
            }
        }
        public static string FormatPrice(double? price)
        {
            if (price.HasValue && price != 0)
            {
                string result = price.Value.ToString("#,#");
                if (result.Contains(","))
                    result = result.Replace(',', '.');
                return result;
            }
            return "0";
        }
        public static string GetDateMonth(DateTime date)
        {
            var result = date.ToString("dd/MM");
            return result;
        }
        public static string TrimLength(string input, int maxLength)
        {
            if (input.ToString() != "")
            {
                if (input.Length > maxLength)
                {
                    maxLength -= "...".Length;
                    maxLength = input.Length < maxLength ? input.Length : maxLength;
                    bool isLastSpace = input[maxLength] == ' ';
                    string part = input.Substring(0, maxLength);
                    if (isLastSpace)
                        return part + "...";
                    int lastSpaceIndexBeforeMax = part.LastIndexOf(' ');
                    if (lastSpaceIndexBeforeMax == -1)
                        return part + "...";
                    return input.Substring(0, lastSpaceIndexBeforeMax) + "...";
                }
                return input;
            }
            else
            {
                return input;
            }
        }
        
    }
}