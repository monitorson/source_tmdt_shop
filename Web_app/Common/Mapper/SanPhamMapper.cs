﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_app.Models;
using AutoMapper;
using DataAccess.Entities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
namespace DailyOpt_CROP4A.Common.Mapper
{
    public class SanPhamMapper
    {
        public static SanPhamViewModel ConvertSanPhamEntityToModel(SanPhamEntity entity)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SanPhamEntity, SanPhamViewModel>();
            });
            SanPhamViewModel model = config.CreateMapper().Map<SanPhamViewModel>(entity);
            return model;
        }

        public static List<SanPhamViewModel> ConvertListSanPhamEntityToModel(List<SanPhamEntity> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SanPhamEntity, SanPhamViewModel>();
            });
            List<SanPhamViewModel> listModel = config.CreateMapper().Map<List<SanPhamViewModel>>(lstEntity);
            return listModel;
        }

        public static SanPhamEntity ConvertSanPhamModelToEntity(SanPhamViewModel model)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SanPhamViewModel, SanPhamEntity>();
            });
            SanPhamEntity entity = config.CreateMapper().Map<SanPhamEntity>(model);
            return entity;
        }

        public static List<SanPhamEntity> ConvertListSanPhamModelToEntity(List<SanPhamViewModel> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SanPhamViewModel, SanPhamEntity > ();
            });
            List<SanPhamEntity> listEntity = config.CreateMapper().Map<List<SanPhamEntity>>(lstEntity);
            return listEntity;
        }
    }
}