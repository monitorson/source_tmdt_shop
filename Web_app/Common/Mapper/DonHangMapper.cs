﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_app.Models;
using AutoMapper;
using DataAccess.Entities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
namespace DailyOpt_CROP4A.Common.Mapper
{
    public class DonHangMapper
    {
        public static DonHangViewModel ConvertDonHangEntityToModel(DonHangEntity entity)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DonHangEntity, DonHangViewModel>();
            });
            DonHangViewModel model = config.CreateMapper().Map<DonHangViewModel>(entity);
            return model;
        }

        public static List<DonHangViewModel> ConvertListDonHangEntityToModel(List<DonHangEntity> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DonHangEntity, DonHangViewModel>();
            });
            List<DonHangViewModel> listModel = config.CreateMapper().Map<List<DonHangViewModel>>(lstEntity);
            return listModel;
        }

        public static DonHangEntity ConvertDonHangModelToEntity(DonHangViewModel model)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DonHangViewModel, DonHangEntity>();
            });
            DonHangEntity entity = config.CreateMapper().Map<DonHangEntity>(model);
            return entity;
        }

        public static List<DonHangEntity> ConvertListDonHangModelToEntity(List<DonHangViewModel> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DonHangViewModel, DonHangEntity > ();
            });
            List<DonHangEntity> listEntity = config.CreateMapper().Map<List<DonHangEntity>>(lstEntity);
            return listEntity;
        }
    }
}