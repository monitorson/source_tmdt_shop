﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_app.Models;
using AutoMapper;
using DataAccess.Entities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
namespace DailyOpt_CROP4A.Common.Mapper
{
    public class NhomSanPhamMapper
    {
        public static NhomSanPhamViewModel ConvertNhomSanPhamEntityToModel(NhomSanPhamEntity entity)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NhomSanPhamEntity, NhomSanPhamViewModel>();
            });
            NhomSanPhamViewModel model = config.CreateMapper().Map<NhomSanPhamViewModel>(entity);
            return model;
        }

        public static List<NhomSanPhamViewModel> ConvertListNhomSanPhamEntityToModel(List<NhomSanPhamEntity> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<NhomSanPhamEntity, NhomSanPhamViewModel>();
            });
            List<NhomSanPhamViewModel> listModel = config.CreateMapper().Map<List<NhomSanPhamViewModel>>(lstEntity);
            return listModel;
        }

        public static NhomSanPhamEntity ConvertNhomSanPhamModelToEntity(NhomSanPhamViewModel model)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NhomSanPhamViewModel, NhomSanPhamEntity>();
            });
            NhomSanPhamEntity entity = config.CreateMapper().Map<NhomSanPhamEntity>(model);
            return entity;
        }

        public static List<NhomSanPhamEntity> ConvertListNhomSanPhamModelToEntity(List<NhomSanPhamViewModel> lstEntity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<NhomSanPhamViewModel, NhomSanPhamEntity > ();
            });
            List<NhomSanPhamEntity> listEntity = config.CreateMapper().Map<List<NhomSanPhamEntity>>(lstEntity);
            return listEntity;
        }
    }
}