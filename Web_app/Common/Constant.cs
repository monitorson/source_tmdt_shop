﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web_app.Common
{
    public class Constant : ApiController
    {
        public const string POSITION_LOGO = "1";

        public const string POSITION_SLIDE = "2";

        public const string POSITION_LOGO_FOOTER = "3";

        public const string POSITION_QC_TRAI = "4";

        public const string POSITION_QC_PHAI = "5";

        public const string POSITION_DOI_TAC = "6";

        public const string POSITION_MENU_TOP = "1";

        public const string POSITION_MENU_BOTTOM = "2";

        public const string POSITION_MENU_LEFT = "3";

        public const string POSITION_MENU_RIGHT = "4";

        public const string POSITION_RIGHT = "2";

        public const string POSITION_LEFT = "3";

        public const string POSITION_HOME = "5";
    }
}