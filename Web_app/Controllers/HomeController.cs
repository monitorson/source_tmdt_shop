﻿using Web_app.Utilities;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Linq;
using Web_app.Common;
using DataAccess.Repositories;
using Web_app.Models;
using System.Collections.Generic;
using System;
using System.Text;
using DataAccess.Entities;
using Services.Imp;

using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Data;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using DailyOpt_CROP4A.Common.Mapper;

namespace Web_app.Controllers
{
    public class HomeController : Controller
    {
        private ConfigRepository _configRepository = null;
        private AdvertisementRepository _advertisementRepository = null;
        private MenuRepository _menuRepository = null;
        private ISanPhamRepository _sanPhamRepository = null;
        public HomeController(ConfigRepository configRepository, AdvertisementRepository advertisementRepository,
            MenuRepository menuRepository, ISanPhamRepository sanPhamRepository)
        {
            _configRepository = configRepository;
            _advertisementRepository = advertisementRepository;
            _menuRepository = menuRepository;
            _sanPhamRepository = sanPhamRepository;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var entiyConfig = _configRepository.GetInfo();
            var model = new ConfigViewModel();
            model.Company = entiyConfig.Company;
            model.SeoKeyWord = entiyConfig.SeoKeyWord;
            model.SeoDesciption = entiyConfig.SeoDesciption;
            model.SeoTitle = entiyConfig.SeoTitle;
            //var title = entiyConfig.SeoTitle != null ? entiyConfig.SeoTitle : entiyConfig.Company;
            //var description = entiyConfig.SeoDesciption != null ? entiyConfig.SeoDesciption : entiyConfig.Company;
            //var key = entiyConfig.SeoKeyWord != null ? entiyConfig.SeoKeyWord : entiyConfig.Company;
            //ViewBag.Title = title;
            //ViewBag.Description = description;
            //ViewBag.Keyword = key;
            return View(model);
        }
        
      
        public ActionResult Footer()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult MenuMobile()
        {
            var entity = _menuRepository.GetByPosition(Constant.POSITION_MENU_TOP);
            return View(entity);
        }
        public ActionResult TimKiem()
        {
            return View();
        }
        public ActionResult Logo()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_LOGO);
            return View(entity);
        }

        public ActionResult Menu()
        {
            var entity = _menuRepository.GetByPosition(Constant.POSITION_MENU_TOP);
            return View(entity);
        }
        public ActionResult Social()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult DieuKhoan()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult SocialFooter()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult Slide()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_SLIDE);
            return View(entity);
        }
        public ActionResult Codebody()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult CodeHeader()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult PageDetail(string Id)
        {
            var model = _menuRepository.GetByNo(Id);
            return View(model);
        }
        public ActionResult SocialRight()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult FanPage()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult RegisterEmail()
        {
            return View();
        }
        public ActionResult DoiTac()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_DOI_TAC);
            return View(entity);
        }
        public ActionResult LienHe()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        [HttpPost]
        public ActionResult SendEmail(string email)
        {
            try
            {
                var infoEmailTo = _configRepository.GetInfo().EmailInfo;
                SendMail(null, email, null, null, infoEmailTo);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpPost]
        public ActionResult Search(string Keyword)
        {

            var listData = _sanPhamRepository.GetListByName(Keyword);
            var lstModel = SanPhamMapper.ConvertListSanPhamEntityToModel(listData);
            return View(lstModel);
        }
        [HttpPost]
        public ActionResult SendEmailContact(string name, string email, string phone, string message)
        {
            try
            {
                var infoEmailTo = _configRepository.GetInfo().EmailInfo;
                SendMail(name, email, phone, message, infoEmailTo);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        static async Task SendMail(string name, string email, string phone, string message, string infoEmailTo)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("phamminhson0994@gmail.com", "Chủ nợ");
            var to = new EmailAddress();
            var subject = "";
            var plainTextContent = "";
            var htmlContent = "";

            if (string.IsNullOrEmpty(name))
            {
                subject = "Thông tin người dùng đăng ký trên trang blog cá nhân";
                to = new EmailAddress(infoEmailTo, "Sơn béo");
                htmlContent = "<strong>Email người dùng đăng ký: </strong> " + email + " ";
            }
            else
            {
                subject = "Thông tin người dùng để lại trên trang liên hệ blog cá nhân";
                to = new EmailAddress(infoEmailTo, "Sơn béo");
                htmlContent = "<strong>Họ tên: </strong> " + name + " <br /><strong>Email người dùng đăng ký: </strong> " + email + "<br /><strong>Số điện thoại: </strong> " + email + " <br /><strong>Nội dung: </strong> " + message + "";
            }
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }


    }
}
