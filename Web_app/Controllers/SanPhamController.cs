﻿using Web_app.Utilities;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Linq;
using Web_app.Common;
using DataAccess.Repositories;
using Web_app.Models;
using System.Collections.Generic;
using System;
using System.Text;
using DataAccess.Entities;
using Services.Imp;

using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Data;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using DailyOpt_CROP4A.Common.Mapper;
using PagedList;

namespace Web_app.Controllers
{
    public class SanPhamController : Controller
    {
        private ConfigRepository _configRepository = null;
        private AdvertisementRepository _advertisementRepository = null;
        private ISanPhamRepository _sanPhamRepository = null;
        private INhomSanPhamRepository _nhomSanPhamRepository = null;
        private IDonHangRepository _donHangRepository = null;
        public SanPhamController(ConfigRepository configRepository, AdvertisementRepository advertisementRepository,
           ISanPhamRepository sanPhamRepository, INhomSanPhamRepository nhomSanPhamRepository, IDonHangRepository donHangRepository)
        {
            _configRepository = configRepository;
            _advertisementRepository = advertisementRepository;
            _sanPhamRepository = sanPhamRepository;
            _nhomSanPhamRepository = nhomSanPhamRepository;
            _donHangRepository = donHangRepository;
        }
        public ActionResult GioHang()
        {
            var lstDatTa = new List<SanPhamViewModel>();
            string[] lstId = null;
            if (Session["GioHang"] != null)
            {
                lstId = Session["GioHang"].ToString().Split(',');
            }
            if (lstId != null)
            {
                var groups = lstId.GroupBy(v => v);
                foreach (var group in groups)
                {
                    var entitySp = _sanPhamRepository.GetByNo(group.Key);
                    entitySp.SoLuong = group.Count().ToString();
                    lstDatTa.Add(SanPhamMapper.ConvertSanPhamEntityToModel(entitySp));
                }
                foreach (var itemsp in lstDatTa)
                {
                    itemsp.NhomSanPham = _nhomSanPhamRepository.GetByNo(itemsp.IdNhomSanPham);
                }
            }
            return View(lstDatTa);
        }
        public ActionResult Set_Gio_Hang(string Id)
        {
            try
            {
                if (Session["GioHang"] != null)
                {
                    Session["GioHang"] = Session["GioHang"].ToString() + "," + Id;
                }
                else
                {
                    Session["GioHang"] = Id;
                }
                return Json(new { Sucsses = "Sucsses" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult CapNhat_Gio_Hang(string[] Id)
        {
            try
            {
                var lstId = string.Join(",", Id);
                Session["GioHang"] = null;
                Session["GioHang"] = lstId;
                return Json(new { Sucsses = "Sucsses" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult ThanhToan()
        {
            var lstDatTa = new List<SanPhamViewModel>();
            string[] lstId = null;
            if (Session["GioHang"] != null)
            {
                lstId = Session["GioHang"].ToString().Split(',');
            }
            if (lstId != null)
            {
                var groups = lstId.GroupBy(v => v);
                foreach (var group in groups)
                {
                    var entitySp = _sanPhamRepository.GetByNo(group.Key);
                    entitySp.SoLuong = group.Count().ToString();
                    lstDatTa.Add(SanPhamMapper.ConvertSanPhamEntityToModel(entitySp));
                }
                foreach (var itemsp in lstDatTa)
                {
                    itemsp.NhomSanPham = _nhomSanPhamRepository.GetByNo(itemsp.IdNhomSanPham);
                }
            }
            return View(lstDatTa);
        }
        public ActionResult NhomSanPhamNoiBat()
        {
            var lstEntity = _nhomSanPhamRepository.GetListNoiBat();
            var lstModel = NhomSanPhamMapper.ConvertListNhomSanPhamEntityToModel(lstEntity);
            foreach (var item in lstModel)
            {
                item.lstSanPham = _sanPhamRepository.GetByIdNhom(item.Id.ToString());
            }
            return View(lstModel);
        }

        public ActionResult DanhSachSanPham(int? page, string type)
        {
            int pagesize = 20;
            int pageNumber = (page ?? 1);
            var idNhomSp = _nhomSanPhamRepository.GetIdByUrl(type);
            var lstSanPhamEntity = _sanPhamRepository.GetByIdNhom(idNhomSp.ToString());
            var lstSanPhamModel = SanPhamMapper.ConvertListSanPhamEntityToModel(lstSanPhamEntity);
            foreach (var item in lstSanPhamModel)
            {
                item.NhomSanPham = _nhomSanPhamRepository.GetByNo(item.IdNhomSanPham);
            }
            return View(lstSanPhamModel.ToPagedList(pageNumber, pagesize));
        }
        public ActionResult SanPhamNoiBat()
        {

            var lstSanPhamEntity = _sanPhamRepository.GetTop10();
            var lstSanPhamModel = SanPhamMapper.ConvertListSanPhamEntityToModel(lstSanPhamEntity);
            foreach (var item in lstSanPhamModel)
            {
                item.NhomSanPham = _nhomSanPhamRepository.GetByNo(item.IdNhomSanPham);
            }
            return View(lstSanPhamModel);
        }
        public ActionResult NhomSanPhamTrangChu()
        {
            var lstEntity = _nhomSanPhamRepository.GetListTrangChu();
            var lstModel = NhomSanPhamMapper.ConvertListNhomSanPhamEntityToModel(lstEntity);
            foreach (var item in lstModel)
            {
                item.lstSanPham = _sanPhamRepository.GetByIdNhom(item.Id.ToString());
            }
            return View(lstModel);
        }
        public ActionResult ChiTietSanPham(string url)
        {
            var sanPhamEntity = _sanPhamRepository.GetByUrl(url);
            var sanPhamModel = SanPhamMapper.ConvertSanPhamEntityToModel(sanPhamEntity);
            sanPhamModel.NhomSanPham = _nhomSanPhamRepository.GetByNo(sanPhamEntity.IdNhomSanPham);
            return View(sanPhamModel);
        }
        public ActionResult SanPhamLienQuan(string IdOld, string IdNhom)
        {
            var lstByNhomSp = _sanPhamRepository.GetByIdNhom(IdNhom);
            var lstSanPhamEntity = lstByNhomSp.Where(c => c.Id != int.Parse(IdOld)).ToList();
            var lstSanPhamModel = SanPhamMapper.ConvertListSanPhamEntityToModel(lstSanPhamEntity);
            foreach (var item in lstSanPhamModel)
            {
                item.NhomSanPham = _nhomSanPhamRepository.GetByNo(item.IdNhomSanPham);
            }
            return View(lstSanPhamModel);
        }
        public ActionResult HoanThanhThanhToan(string Ten, string Sdt, string DiaChi, string GhiChu)
        {
            var lstDatTa = new List<SanPhamViewModel>();
            string[] lstId = null;
            var sucsses = false;
            if (Session["GioHang"] != null)
            {
                lstId = Session["GioHang"].ToString().Split(',');
            }
            if (lstId != null)
            {
                var groups = lstId.GroupBy(v => v);
                foreach (var group in groups)
                {
                    var entitySp = _sanPhamRepository.GetByNo(group.Key);
                    entitySp.SoLuong = group.Count().ToString();
                    lstDatTa.Add(SanPhamMapper.ConvertSanPhamEntityToModel(entitySp));
                }
                foreach (var itemsp in lstDatTa)
                {
                    itemsp.NhomSanPham = _nhomSanPhamRepository.GetByNo(itemsp.IdNhomSanPham);
                    var entityDonHang = new DonHangEntity();
                    entityDonHang.LoaiThanhToan = "Thanh toán khi giao hàng";
                    entityDonHang.NgayMua = DateTime.Now;
                    entityDonHang.Sdt = Sdt;
                    entityDonHang.SoLuong = int.Parse(itemsp.SoLuong);
                    entityDonHang.TenKhachHang = Ten;
                    entityDonHang.TenSanPham = itemsp.TenSanPham;
                    entityDonHang.TongGia = itemsp.GiaKhuyenMai != null ? (double.Parse(itemsp.GiaKhuyenMai) * int.Parse(itemsp.SoLuong)) : (double.Parse(itemsp.GiaBan) * int.Parse(itemsp.SoLuong));
                    entityDonHang.DonGia = double.Parse(itemsp.GiaKhuyenMai != null ? itemsp.GiaKhuyenMai : itemsp.GiaBan);
                    entityDonHang.DiaChi = DiaChi;
                    entityDonHang.GhiChu = GhiChu;
                    entityDonHang.Active = false;
                    sucsses = _donHangRepository.Create(entityDonHang);

                    // send email
                    var infoEmailTo = _configRepository.GetInfo().EmailInfo;
                    SendMail(Ten, Sdt, GhiChu, infoEmailTo, itemsp.TenSanPham, itemsp.SoLuong, entityDonHang.TongGia.ToString());
                }
            }
            if (sucsses)
            {
                Session["GioHang"] = null;
                return Json(new { Sucsses = "Sucsses" }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { Sucsses = "False" }, JsonRequestBehavior.AllowGet); }

        }
        static async Task SendMail(string name, string phone, string GhiChu, string infoEmailTo, string TenSanPham, string SoLuong ,string TongGia)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("support@palletnamson.com", "Info Đơn hàng");
            var to = new EmailAddress();
            var subject = "";
            var plainTextContent = "";
            var htmlContent = "";
            subject = "Thông tin đơn hàng mới";
            to = new EmailAddress(infoEmailTo, "Sơn béo");
            htmlContent = "<strong>Họ tên: </strong> " + name + " <br /><" +
                "strong>Số điện thoại: </strong> " + phone + "<br />" +
                "<strong>Tên sản phẩm: </strong> " + TenSanPham + " <br />" +
                "<strong>Số lượng: </strong> " + SoLuong + " <br />" +
                "<strong>Tổng giá: </strong> " +comon.FormatPrice(decimal.Parse(TongGia)) + " VNĐ<br />" +
                "<strong>Ghi chú: </strong> " + GhiChu + "";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
