﻿using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using DataAccess.Entities;

namespace DailyOpt_CROP4A.Controllers
{
    public class NewsController : Controller
    {
        private NewsRepository _newsRepository = null;
        private GroupNewsRepository _groupNewsRepository = null;
        public NewsController(NewsRepository newsRepository, GroupNewsRepository groupNewsRepository)
        {
            _newsRepository = newsRepository;
            _groupNewsRepository = groupNewsRepository;
        }
        // GET: News
        public ActionResult Search()
        {
            return View();
        }

        public ActionResult GioiThieuCaNhan()
        {
            var model = _newsRepository.GetByPosition(Constant.POSITION_RIGHT);
            if (model == null)
            {
                model = new List<NewsEntity>();
            }
            return View(model);
        }
        public ActionResult TinCotPhai()
        {
            var model = _groupNewsRepository.GetByPosition(Constant.POSITION_RIGHT);
            if (model != null)
            {
                foreach (var item in model)
                {
                    item.ListNews = _newsRepository.GetByGroupNews(item.Id.ToString());
                }
            }
            else
            {
                model = new List<GroupNewsEntity>();
            }


            return View(model);
        }
        private NewsViewModel ParseObjNewsEntityToModel(NewsEntity entity)
        {
            var groupNews = _groupNewsRepository.GetByNo(entity.IdGroupNews.ToString());
            var newsViewModel = new NewsViewModel
            {
                Id = entity.Id,
                Title = entity.Title,
                Url = entity.Url,
                Img = entity.Img,
                DateCreate = entity.DateCreate,
                DateDelete = entity.DateDelete,
                DateUpdate = entity.DateUpdate,
                Description = entity.Description,
                Detail = entity.Detail,
                IdGroupNews = entity.IdGroupNews,
                IsNew = entity.IsNew == true ? 1 : 0,
                OrderNews = entity.OrderNews,
                SeoDesciption = entity.SeoDesciption,
                SeoKeyWord = entity.SeoKeyWord,
                SeoTitle = entity.SeoTitle,
                UrlVideo = entity.UrlVideo,
                UserCreate = entity.UserCreate,
                UserDelete = entity.UserDelete,
                UserUpdate = entity.UserUpdate,
                DisplayGroupNews = groupNews.Name,
                UrlGroupNews = groupNews.Url,
                Position = entity.Position,
                Active = entity.Active == true ? 1 : 0,
            };
            return newsViewModel;
        }
        public ActionResult CotPhai()
        {
            return View();
        }
        public ActionResult NewsHome()
        {
            var data = _groupNewsRepository.GetAll().Where(g => g.Position == Constant.POSITION_HOME).FirstOrDefault();
            if (data != null)
            {
                data.ListNews = _newsRepository.GetByGroupNews(data.Id.ToString());
            }
            else
            {
                data = new GroupNewsEntity();
            }
            return View(data);
        }

        public ActionResult ChiTietBaiViet(string type)
        {
            var entity = _newsRepository.GetAll().Where(c => c.Url.Equals(type.ToLower())).FirstOrDefault();
            var model = ParseObjNewsEntityToModel(entity);
            return View(model);
        }
        public ActionResult BaiVietLienQuan(int IdGroupNews, int IdNews)
        {
            var newitem = _newsRepository.GetAll().Where(c => c.Id != IdNews && c.IdGroupNews == IdGroupNews);
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in newitem)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            return View(listModelNews);
        }

        public ActionResult DanhSachBaiViet(string type)
        {
            var data = _groupNewsRepository.GetAll().Where(g => g.Url.Equals(type)).FirstOrDefault();
            if (data != null)
            {
                data.ListNews = _newsRepository.GetByGroupNews(data.Id.ToString());
            }
            else
            {
                data = new GroupNewsEntity();
            }
            return View(data);
        }
        [HttpPost]
        public ActionResult TimKiem(string key)
        {
            var data = _newsRepository.SearchTitle(key);
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in data)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            return View(listModelNews);
        }

        public ActionResult PartialNews(int id, int page)
        {
            var newsitem = _newsRepository.GetByGroupNews(id.ToString());
            var lst = _newsRepository.NewsPage(page, 6, id.ToString(), newsitem.ToList());
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in lst)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            var currentUrl = Url.Action("PartialNews", "News", new { id = id });
            ViewBag.Htmlpage = new HtmlPager().getHtmlPageForum(currentUrl + "&page=", 1, page, 6, newsitem.Count());
            return View(listModelNews);
        }
    }
}