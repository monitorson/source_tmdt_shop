﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace DailyOpt_CROP4A.Validate
{
    public class CustomStringLength : ValidationAttribute
    {

        public CustomStringLength(int maxLength, params string[] propertyNames)
        {
            this.PropertyNames = propertyNames;
            this.MaxLength = maxLength;
        }

        public string[] PropertyNames { get; private set; }
        public int MaxLength { get; private set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var properties = this.PropertyNames.Select(validationContext.ObjectType.GetProperty);
            var values = properties.Select(p => p.GetValue(validationContext.ObjectInstance, null)).OfType<string>();
            var totalLength = values.Sum(x => x.Length) + GetByteCharacter(Convert.ToString(value));
            if (totalLength > this.MaxLength)
            {
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }

        protected int GetByteCharacter (string value)
        {
            Encoding unicode = Encoding.GetEncoding(932);
            byte[] unicodeBytes = unicode.GetBytes(value);
            int byteValue  = unicodeBytes.Count();
            return byteValue;
        }
    }
}