﻿using DataAccess.Entities;
using System.Collections.Generic;
using System.Web;

namespace Services
{
    public interface IExperimentService
    {
        /// <summary>
        /// </summary>
        /// <param name="experimentEntity"></param>
        /// <param name="qualityMeasuermentEntity"></param>
        /// <returns></returns>
        ExperimentEntity Create(ExperimentEntity experimentEntity, IList<QualityMeasurementEntity> qualityMeasuermentEntity);

        /// <summary>
        /// Update a Item
        /// </summary>
        /// <param name="itemEntity"></param>
        /// <param name="itemDetailEntity"></param>
        /// <param name="itemImageEntities"></param>
        /// <param name="itemTagEntities"></param>
        /// <param name="itemMemoEntity"></param>
        /// <param name="itemLabelEntity"></param>
        /// <returns></returns>
        ExperimentEntity Update(ExperimentEntity experimentEntity, IList<QualityMeasurementEntity> qualityMeasuermentEntity);
    }
}